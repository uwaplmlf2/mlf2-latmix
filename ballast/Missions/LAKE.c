/* arch-tag: 0e0de6c5-8b15-4b25-8b35-21d785e9076f */
/* LAKE WASHINGTON - includes BALLAST,  EOS and TURBULENCE components */
/* NEED TO SET PARAMETERS SET IN InitializePUGETSOUNDBALLASTEOS */
/* Nov 7, 2010  */

/* Mission consists of
  stage==3:  Initial fast profile, followed by Mixed layer drift
  stage==2:  Puget Sound ballast, followed by EOS
  else :          Puget Sound ballast only, if this has already happened, then end mission
  Program can switch between EOS, drift or terminate at each COMM
*/
int next_mode(int nmode, double day_time)
{
    static int icall=0;
    int oldmode,i;
    double x;
    oldmode=nmode;
    if (nmode==MODE_START){
	log_event("next_mode:LAKE Mission Start\n");
	nmode=MODE_PROFILE_DOWN;
	Steps.cycle=0;
	return(nmode);
    }

    ++icall;
    switch(icall){
        case 1: nmode=MODE_PROFILE_DOWN; 
		if (stage!=3){  /* normal PSBallast check out */
			save1=Settle.timeout;/* captive dive  - short */
			Settle.timeout=Steps.time0;
		}
		else{  /* Stage=3 - Quick full depth profile */
			Down.Pmax=Steps.z5; 
			icall=8; /* Skip ballasting  -HACK ALERT - UGLY - UGLY  */
		}
		break;
        case 2: nmode=MODE_SETTLE;
		Ballast.Vset=0;  /* do not set ballast.v0 based on this settle */
		break;
        case 3: nmode=MODE_DRIFT_SEEK;
		Ballast.Vset=1;  /* restore value */
		save2=Drift.timeout_sec;
		save3=Drift.closed_time;
		Drift.timeout_sec=Drift.time2;   /* very short test drift mode */
		Drift.closed_time=Drift.timeout_sec+100.; /* don't open drogue in captive dive */
		break;
	case 4: nmode=MODE_PROFILE_UP;
		Drift.timeout_sec=save2;
		Drift.closed_time=save3;
		break;
	case 5: nmode=MODE_COMM;break;      /* Option to terminate here or remove linefloat */
	case 6: nmode=MODE_PROFILE_DOWN; 
		Settle.timeout=save1; /* restore long settle */
		Settle.Ptarget=Steps.z4; /* Pressure goal if necessary */
		break;
        case 7: nmode=MODE_SETTLE;break;
        case 8: nmode=MODE_DRIFT_SEEK;
		save2=Drift.timeout_sec;
		save3=Drift.closed_time;
		Drift.timeout_sec=1000.;  /* longer test drift mode */
		Drift.closed_time=200.;           /* open part way through */
		break;
	case 9: nmode=MODE_PROFILE_UP;   /*  stage=3 re-enters here from above & below */
		if (Drift.timeout_sec==1000.){     /* HACK ALERT -  UGLY  UGLY UGLY  */
			Drift.timeout_sec=save2;   /* restore drift timeouts */
			Drift.closed_time=save3;
		}
		log_event("Check Drift timeout %f %f\n",Drift.timeout_sec, Drift.closed_time);
		break;
	case 10: nmode=MODE_COMM;break;   /* do another cycle - can reset stage */
	case 11: if (stage!=2 & stage!=3){        /* Choose next cycle based on stage */
	/*** if stage not 2 or 3 after COMM end mission */
			nmode=MODE_DONE;
		}
        /****if stage==3, then mission continues as Turbulence */
		else if (stage==3){
			nmode=MODE_DRIFT_ML;
			log_event("next_mode: LAKE DRIFT START\n");
			icall=8;  /* next mode after drift is is UP */
		}
         /*** if stage==2, then mission continues as EOS  */
		else {
			log_event("next_mode: LAKE EOS START (after Ballast)\n");
			nmode=MODE_PROFILE_UP;   /*initial up */
			Up.surfacetime=100.;  /* add a few surface points*/
		}
		break;
	case 12: nmode=MODE_PROFILE_DOWN;
		Mlb.point=0;  /* initialize recorder */
		Mlb.record=1;  /* Start recording */
		Down.Pmax=Steps.z5;   /* Ready for full depth DOWN profile */
		log_event("Start Recording downcast \n");
		break;
	case 13: nmode=MODE_SETTLE;   /* Settle #1 */
		Mlb.record=0.;
		log_event("Finish Recording downcast \n");
		pMlb=&Mlb;
		x=z2sigma(pMlb,Steps.z4);  /* Get new target from saved profile */
		if (x>0){
			Ballast.Target=x;
			log_event("Settle Target#1: %6.3f db %6.3f sigma\n",Steps.z4,x-1000);
		}
		else {    /* if z2sigma() fails use Ballast.Target from initial ballasting */
			log_event("ERROR: next_mode No new target#1, use %6.3g\n",Ballast.Target-1000.);
			}
		Settle.Ptarget=Steps.z4; /* Pressure goal if necessary */
		Settle.SetTarget=2;  /* set target to Ballast.Target */
		Settle.timeout=Steps.time4; 
		Settle.seek_time=Settle.timeout/2.;  /* half of seeking at full power */
		Settle.decay_time=Settle.seek_time*0.6;  /* decay seeking to 20% by end */
		break;
	case 14: nmode=MODE_SETTLE;   /* Settle #2 */
		pMlb=&Mlb;
		x=z2sigma(pMlb,Steps.z3);  /* Get new target from saved profile */
		if (x>0){
			Ballast.Target=x;
			log_event("Settle Target#2: %6.3f db %6.3f sigma\n",Steps.z3,x-1000.);
		}
		else {
			log_event("ERROR: next_mode No new target#2, use %6.3g\n",Ballast.Target-1000.);
			}
		Settle.Ptarget=Steps.z3; /* Pressure goal if necessary */
		Settle.SetTarget=2;
		Settle.timeout=Steps.time3; 
		Settle.seek_time=Settle.timeout/2.;  /* half of seeking at full power */
		Settle.decay_time=Settle.seek_time*0.6;  /* decay seeking to 20% by end */
		break;
	case 15: nmode=MODE_SETTLE;   /* Settle #3 */
		pMlb=&Mlb;
		x=z2sigma(pMlb,Steps.z2);  /* Get new target from saved profile */
		if (x>0){
			Ballast.Target=x;
			log_event("Settle Target#3: %6.3f db %6.3f sigma\n",Steps.z2,x-1000.);
		}
		else {
			log_event("ERROR: next_mode No new target#3, use %6.3g\n",Ballast.Target-1000.);
			}
		Settle.Ptarget=Steps.z2; /* Pressure goal if necessary */
		Settle.SetTarget=2;
		Settle.timeout=Steps.time2; 
		Settle.seek_time=Settle.timeout/2.;  /* half of seeking at full power */
		Settle.decay_time=Settle.seek_time*0.6;  /* decay seeking to 20% by end */
		break;
	case 16: nmode=MODE_SETTLE;   /* Settle #4 */
		pMlb=&Mlb;
		x=z2sigma(pMlb,Steps.z1);  /* Get new target from saved profile */
		if (x>0){
			Ballast.Target=x;
			log_event("Settle Target#4: %6.3f db %6.3f sigma\n",Steps.z1,x-1000.);
		}
		else {
			log_event("ERROR: next_mode No new target#4, use %6.3f\n",Ballast.Target-1000.);
			}
		Settle.Ptarget=Steps.z1; /* Pressure goal if necessary */
		Settle.SetTarget=2;
		Settle.timeout=Steps.time1; 
		Settle.seek_time=Settle.timeout/2;
		Settle.decay_time=Settle.seek_time*0.6;  
		break;
	case 17: nmode=MODE_PROFILE_UP;   /*Ready for next cycle stage=2*/
		icall=9;   /* back to COMM */
		break;
      default: nmode=MODE_ERROR;
		log_event("ERROR: next_mode Cant get here %d\n",icall);
		break;  /* can't get here */
    }
    log_event("next_mode: %d ->%d\n",oldmode,nmode);
    return(nmode);
}

#include "SampleLAKE.c"
