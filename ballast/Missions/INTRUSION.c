/* arch-tag: ed92ebd7-0589-42ed-9798-894b274fc4b0 */
/* INTRUSION MISSION - GO TO ISOPYCNAL - STAY THERE
*/

/* Subroutine to handle mode changes
both routine and due to acoustic commands
PARAMETERS set Initialize AESOP.c */

int next_mode(int nmode, double day_time)
{
double x;
int static skip_surface=0;
static int icall=0; 


/* command handling - do_command, command_end is a global variable */
if (do_command>0){
	if (do_command==1){  /* SURFACE NOW */
		log_event("Execute Command 1 - surface\n");
		command_end=1;  /* stop present modes */
	}
	else if (do_command==2){   /* lighter - no mode change */
		/* Ballast.rho0=Ballast.rho0+telem_Step;
		log_event("Execute Command 2 - DOWN.  New target %8.4f\n",Ballast.rho0);*/
		Drift.iso_target=Drift.iso_target+telem_Step;
		log_event("Execute Command 2 - DOWN.  New target %8.4f\n",Drift.iso_target);
	}
	else if (do_command==3){   /* heavier - no mode change */
		/* Ballast.rho0=Ballast.rho0-telem_Step;
		log_event("Execute Command 3 - UP.  New target %8.4f\n",Ballast.rho0);*/
		Drift.iso_target=Drift.iso_target-telem_Step;
		log_event("Execute Command 3 - UP.  New target %8.4f\n",Drift.iso_target);
	}	
	else {
		nmode=MODE_ERROR;
	}
/* During cruise fix!!!!
if (do_command==2 || do_command==3){
	Drift.iso_target=Ballast.rho0; /* Set Drift target here  
	Down.Sigmax=Ballast.rho0;
	log_event("Set targets to new value\n");
} */
do_command=0;
return(nmode);  /* No mode change yet- act through command_end */
}

if (nmode==MODE_START){
	Drift.iso_target=Ballast.rho0; /* Set Drift target here  */
	Down.Sigmax=Ballast.rho0;
	log_event("INTRUSION MISSION  target=%f\n",Ballast.rho0-1000.);

	nmode=MODE_PROFILE_DOWN;
	icall=0;
	return(nmode);
} 
else if (nmode==MODE_ERROR){ /* this should not happen since errors cause COMM */
	log_event("ERROR MODE at wierd time- reset loop at COMM\n");
	icall=4;  /* reset loop */	
}
else if (nmode==MODE_COMM && icall%5 !=0 ){  /* Error or Return from error */
log_event("Comm at wrong time - reset mode loop mode\n");
icall=0;  /* reset loop */
}

printf("NB Mode %d ->",nmode);

switch(icall%5){
	case 0: nmode=MODE_PROFILE_DOWN; break;
	case 1: nmode=MODE_SETTLE;break;
	case 2: nmode=MODE_DRIFT_SEEK;break;
	case 3:  nmode=MODE_PROFILE_UP;
		command_end=0;   /* reset mode ending command */
		break;
	case 4: nmode=MODE_COMM;
		command_end=0;   /* make sure in case another command happens */
		break;
	default: nmode=MODE_ERROR;break;  /* can't get here */
}
printf("%d\n",nmode);
++icall;
return(nmode);
}
