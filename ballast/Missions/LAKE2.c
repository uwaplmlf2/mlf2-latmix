/* arch-tag: 0e0de6c5-8b15-4b25-8b35-21d785e9076f */
/* LAKE2 from LATMIX  */
/* NEED TO SET PARAMETERS SET IN InitializeLATMIX */
/* Sep 12, 2011  */

/* Mission consists of
 stage==0:  Puget Sound ballast, followed by EOS
 else :          Lagrangian in ML immediately, COMM repeat
 
 Add StageSim structure to simulate satellite stage changes
 
 Includes BART board commands to end mission. Ignore change isopycnal command
 */

int next_mode(int nmode, double day_time)
{
    static int icall=0;
	static int oldstage=0;
    int oldmode,i;
    double x;
    oldmode=nmode;
	
	if (stage!=0 && stage!=1 && stage!=2){
		nmode=MODE_ERROR;
		log_event("stage is not a valid value %d\n",stage);
	}
	
	/* command handling - do_command, command_end is a global variable */
	if (do_command>0){
		if (do_command==1){  /* SURFACE NOW */
			log_event("Execute Command 1 - surface in emergency\n");
			nmode=MODE_ERROR;
		}
		/*  Comment out ballast change command
		 else if (do_command==2){   /* lighter - no mode change 
		 Ballast.Target=Ballast.Target+telem_Step; 
		 log_event("Execute Command 2 - DOWN.  New target %8.4f\n",Ballast.Target); 
		 }
		 else if (do_command==3){   /* heavier - no mode change 
		 Ballast.Target=Ballast.Target-telem_Step; 
		 log_event("Execute Command 3 - UP.  New target %8.4f\n",Ballast.Target);
		 }	
		 */
		else {
			nmode=MODE_ERROR;
		}
		do_command=0;
		return(nmode);  /* No mode change yet- act through command_end */
	}
	
	if (stage==0 ){  /* normal PSBallast Followed by EOS */
		
		if (nmode==MODE_START || stage!=oldstage){
			log_event("next_mode:LAKE2 Mission Start - BALLAST \n");
			nmode=MODE_PROFILE_DOWN;
			oldstage=stage;
			icall=0;
			return(nmode);
		}
		++icall;
		switch(icall){
				/* --------------------- INITIAL DIVE ---------------   */
			case 1: nmode=MODE_PROFILE_DOWN; 
				save1=Settle.timeout;               /* Initial captive dive  - short */
				Settle.timeout=Steps.time0;
				break;
			case 2: nmode=MODE_SETTLE;
				Ballast.Vset=0;  /* do not set ballast.v0 based on this settle */
				Ballast.Target=PotDensity;   /* set settle target at end of dive */
				break;
			case 3: nmode=MODE_DRIFT_SEEK;
				Ballast.Vset=1;  /* restore value */
				save2=Drift.timeout_sec;
				save3=Drift.closed_time;
				Drift.timeout_sec=Drift.time2;   /* very short test drift mode */
				Drift.closed_time=Drift.timeout_sec+100.; /* don't open drogue in captive dive */
				break;
			case 4: nmode=MODE_PROFILE_UP;
				Drift.timeout_sec=save2;
				Drift.closed_time=save3;
				break;
			case 5: nmode=MODE_COMM;
				break;      /* Option to terminate here or remove linefloat */
				
				/**** CONTINUE AS BALLAST MISSION */
				
			case 6: nmode=MODE_PROFILE_DOWN; 
				Settle.timeout=save1; /* restore long settle */
				Settle.Ptarget=Steps.z4; /* Pressure goal if necessary */
				break;
			case 7: 
				nmode=MODE_SETTLE;
				Ballast.Target=PotDensity;   /* set settle target at end of dive */
				break;
			case 8: nmode=MODE_DRIFT_SEEK;
				save2=Drift.timeout_sec;
				save3=Drift.closed_time;
				Drift.timeout_sec=1000.;  /* longer test drift mode */
				Drift.closed_time=200.;           /* open part way through */
				break;
			case 9: nmode=MODE_PROFILE_UP;   /*  stage=3 re-enters here from above & below */
				Drift.timeout_sec=save2;   /* restore drift timeouts */
				Drift.closed_time=save3;
				break;
			case 10: nmode=MODE_COMM;
			    stage=1;  /* continue as EOS */
				break;   /* do another cycle - can reset stage */
				
			default: nmode=MODE_ERROR;
				log_event("ERROR BALLAST: next_mode Cant get here %d\n",icall);
				break;  /* can't get here */
		}  /* End Stage=0 loop */
	}
	
	/* ------------------ EOS ----------------  */
	if (stage==1 ){ 
		if (nmode==MODE_START || stage!=oldstage){
			log_event("next_mode:LAKE2 Mission - EOS \n");
			nmode=MODE_PROFILE_DOWN;
			Down.Pmax=0;
			oldstage=stage;
			icall=0;
			return(nmode);
		}
		++icall;
		switch(icall){				
			case 1: nmode=MODE_PROFILE_UP;   /*initial up */
				break;
			case 2: nmode=MODE_PROFILE_DOWN;
				Mlb.point=0;  /* initialize recorder */
				Mlb.record=1;  /* Start recording */
				Down.Pmax=Steps.z5;   /* Ready for full depth DOWN profile */
				log_event("Start Recording downcast \n");
				break;
			case 3: nmode=MODE_SETTLE;   /* Settle #1 */
				Mlb.record=0.;
				log_event("Finish Recording downcast \n");
				pMlb=&Mlb;
				x=z2sigma(pMlb,Steps.z4);  /* Get new target from saved profile */
				if (x>0){
					Ballast.Target=x;
					log_event("Settle Target#1: %6.3f db %6.3f sigma\n",Steps.z4,x-1000);
				}
				else {    /* if z2sigma() fails use Ballast.Target from initial ballasting */
					log_event("ERROR: next_mode No new target#1, use %6.3g\n",Ballast.Target-1000.);
				}
				Settle.Ptarget=Steps.z4; /* Pressure goal if necessary */
				Settle.SetTarget=2;  /* set target to Ballast.Target */
				Settle.timeout=Steps.time4; 
				Settle.seek_time=Settle.timeout/2.;  /* half of seeking at full power */
				Settle.decay_time=Settle.seek_time*0.6;  /* decay seeking to 20% by end */
				break;
			case 4: nmode=MODE_SETTLE;   /* Settle #2 */
				pMlb=&Mlb;
				x=z2sigma(pMlb,Steps.z3);  /* Get new target from saved profile */
				if (x>0){
					Ballast.Target=x;
					log_event("Settle Target#2: %6.3f db %6.3f sigma\n",Steps.z3,x-1000.);
				}
				else {
					log_event("ERROR: next_mode No new target#2, use %6.3g\n",Ballast.Target-1000.);
				}
				Settle.Ptarget=Steps.z3; /* Pressure goal if necessary */
				Settle.SetTarget=2;
				Settle.timeout=Steps.time3; 
				Settle.seek_time=Settle.timeout/2.;  /* half of seeking at full power */
				Settle.decay_time=Settle.seek_time*0.6;  /* decay seeking to 20% by end */
				break;
			case 5: nmode=MODE_SETTLE;   /* Settle #3 */
				pMlb=&Mlb;
				x=z2sigma(pMlb,Steps.z2);  /* Get new target from saved profile */
				if (x>0){
					Ballast.Target=x;
					log_event("Settle Target#3: %6.3f db %6.3f sigma\n",Steps.z2,x-1000.);
				}
				else {
					log_event("ERROR: next_mode No new target#3, use %6.3g\n",Ballast.Target-1000.);
				}
				Settle.Ptarget=Steps.z2; /* Pressure goal if necessary */
				Settle.SetTarget=2;
				Settle.timeout=Steps.time2; 
				Settle.seek_time=Settle.timeout/2.;  /* half of seeking at full power */
				Settle.decay_time=Settle.seek_time*0.6;  /* decay seeking to 20% by end */
				break;
			case 6: nmode=MODE_SETTLE;   /* Settle #4 */
				pMlb=&Mlb;
				x=z2sigma(pMlb,Steps.z1);  /* Get new target from saved profile */
				if (x>0){
					Ballast.Target=x;
					log_event("Settle Target#4: %6.3f db %6.3f sigma\n",Steps.z1,x-1000.);
				}
				else {
					log_event("ERROR: next_mode No new target#4, use %6.3f\n",Ballast.Target-1000.);
				}
				Settle.Ptarget=Steps.z1; /* Pressure goal if necessary */
				Settle.SetTarget=2;
				Settle.timeout=Steps.time1; 
				Settle.seek_time=Settle.timeout/2;
				Settle.decay_time=Settle.seek_time*0.6;  
				break;
			case 7: nmode=MODE_PROFILE_UP;   /*Ready for next cycle */
				break;
			case 8: nmode=MODE_COMM;
				icall=0;
				break; 
			default: nmode=MODE_ERROR;
				log_event("ERROR EOS: next_mode Cant get here %d\n",icall);
				break;  /* can't get here */
		}  /* End Stage=1 loop */
	}
	
	if(stage==2) {  /* -----------------   TURBULENCE MISSION -------- */
		if (nmode==MODE_START || stage!=oldstage){
			log_event("next_mode:LAKE2 Mission Start - TURBULENCE \n");
			nmode=MODE_PROFILE_DOWN;
			Down.Pmax=5;
			icall=0;
			oldstage=stage;
			return(nmode);
		}
		
		++icall;
		switch(icall){
			case 1: nmode=MODE_DRIFT_ML;
				break;
			case 2: nmode=MODE_PROFILE_UP;
				break;
			case 3: nmode=MODE_COMM;
				icall=0;
				break;  
			default: nmode=MODE_ERROR;
				log_event("ERROR TURBULENCE: next_mode Cant get here %d\n",icall);
				break;  /* can't get here */
		}
	} /* end stage==2 loop */
	log_event("next_mode: %d ->%d\n",oldmode,nmode);
	
#ifdef SIMULATION
	/* Simulate satellite change of stage */
	/* switch between new and old every jump COMM modes */
	if (nmode==MODE_COMM){
		log_event("COMM mode\n");
		log_event("count %d  jump %d stage %d\n",Stagesim.count, Stagesim.jump,stage);
		++Stagesim.count;
		if (Stagesim.count>=Stagesim.jump){
			log_event("CHANGE STAGE: %d ->",stage);
			if (stage!=Stagesim.stage1 ){
				stage=Stagesim.stage1;
				Stagesim.count=0;
			}
			else{
				stage=Stagesim.stage2;
				Stagesim.count=0;
			}
			log_event("%d\n",stage);
		}
	}
#endif
	return(nmode);
}
#include "SampleLAKE2.c"

