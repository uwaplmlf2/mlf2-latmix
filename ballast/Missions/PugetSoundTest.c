/* arch-tag: 4f3a5780-06d7-4c3c-9a05-7cfeb7b27419 */
/* PUGET SOUND TEST MISSION  - May 2006 */
/* MEANT FOR USE WITH FLOAT ON STRING ONLY!! */

int next_mode(int nmode, double day_time)
{
    if (nmode==MODE_START){
	printf(" Initial wavemode call\n");
	nmode=MODE_PROFILE_DOWN;
	return(nmode);
    }

    printf("PS Mode %d ->",nmode);
    ++icall;
    switch(icall){
        case 1: nmode=MODE_PROFILE_DOWN; 
	    break;
	case 2: nmode=MODE_DRIFT_ISO;
		save1=Drift.timeout;
		Drift.timeout=10.*60./86400;  /* Very short test */
		break;
	case 3: nmode=MODE_PROFILE_UP;break;
	case 4: nmode=MODE_COMM;break;      /* Option to terminate here */
	case 5: nmode=MODE_PROFILE_DOWN; 
	    Drift.timeout=save1; /* longer test */
	    break;
        case 6: nmode=MODE_DRIFT_ISO;break;
	case 7: nmode=MODE_PROFILE_UP;break;
	case 8: nmode=MODE_COMM;break;   /* Pick up float here */
	case 9: nmode=MODE_DONE;break;
        default: nmode=MODE_ERROR;break;  /* can't get here */
    }
    printf("%d\n",nmode);
    return(nmode);
}