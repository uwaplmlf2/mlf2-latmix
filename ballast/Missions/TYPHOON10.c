/* arch-tag: ed92ebd7-0589-42ed-9798-894b274fc4b0 */
/* TYPHOON 2010 MISSION 
ADD sampling() subroutine
*/

int next_mode(int nmode, double day_time)
{
double x;
static int icall=0;
static int cycle=17;

if (nmode==MODE_START){
	if (stage==1){
		log_event("TYPHOON10  >> GAS <<  FLOAT MISSION\n");
	}
	else if (stage==2) {
		log_event("TYPHOON10  >> VECTOR <<  FLOAT MISSION\n");
	}
	else if (stage==3){
		log_event("TYPHOON10  >> SKIP STORM GAS <<  FLOAT MISSION\n");
	}
	else {  /*Gas by default */
		log_event("STAGE ERROR 1 - Default to TYPHOON10 GAS  \n");
		stage=1;
	}
	icall=0;
	nmode=MODE_PROFILE_DOWN;   /*initial down/up */
	Down.Pmax=120.;
	DISABLE_SENSORS(MODE_PROFILE_DOWN, SENS_ANR);
	DISABLE_SENSORS(MODE_PROFILE_UP, SENS_ANR);
	return(nmode);
	} 
else if (nmode==MODE_ERROR){ /* this should not happen since errors cause COMM */
	log_event("ERROR MODE at wierd time \n");
	return(nmode);  /* return with error */	
	}
else if (nmode==MODE_COMM && (icall%cycle !=1 || icall%cycle !=14) ){  /* Error or Return from error */
log_event("Comm at wrong time - do nothing \n");
	}
	
if (stage<1 || stage>3){
	stage=1;
	log_event("STAGE ERROR 2 - Default to stage=1, GAS MISSION  \n");
}
	
printf("icall=%d :  Mode %d ->",icall,nmode);

/* Choose between wake and storm segments */
if ( icall<30 ) {  
	/* STORM SEGMENT  - SAME FOR ALL FLOAT TYPES */
	switch(icall%cycle){
		case 0: nmode=MODE_PROFILE_UP;break;
		case 1: nmode=MODE_COMM;
			if (stage==3){
				icall=icall+cycle;  /* Skip storm segment */
			}
			break;
		case 2: nmode=MODE_SETTLE;   /* Settle0 - surface */
			Settle.SetTarget=0;  /* don't autoset */
			Ballast.Vset=0;  /* don't set Vol0 from surface */
			Settle.Target=PotDensity-10;  /* stay on surface */
			Settle.timeout=Steps.time0;
			Settle.seek_time=Settle.timeout;
			break;
		case 3: nmode=MODE_SETTLE;  /* settle 1 - below MLB */
			Ballast.Vset=1;  /* Vol0 set if good settle */
			Steps.Sig0=PotDensity;  /* Reference surface density */
			Settle.Target=Steps.Sig0+Steps.dSig1;  /* Just below ML */
			Settle.timeout=Steps.time1;
			Settle.seek_time=Settle.timeout;
			break;
		case 4: nmode=MODE_PROFILE_DOWN;    
			Steps.z1=PressureG;   /* Reference pressure at end of previous settle*/
			Down.Pmax=Steps.z1+(Steps.z4-Steps.z1)/4;
			break;
		case 5: nmode=MODE_SETTLE;   /* Settle 2 - Tcline */
			Settle.SetTarget=2;  /* target = ballast.Target */
			Settle.timeout=Steps.time2;
			Settle.seek_time=Settle.timeout;
			break;
		case 6: nmode=MODE_PROFILE_DOWN;    
			Down.Pmax=Steps.z1+(Steps.z4-Steps.z1)*2/4;
			break;
		case 7: nmode=MODE_SETTLE;   /* Settle 3 - Tcline */
			Settle.timeout=Steps.time3;
			Settle.seek_time=Settle.timeout;
			break;
		case 8: nmode=MODE_PROFILE_DOWN;    
			Down.Pmax=Steps.z1+(Steps.z4-Steps.z1)*3/4;
			break;
		case 9: nmode=MODE_SETTLE;   /* Settle 4 - Tcline */
			Settle.timeout=Steps.time4;
			Settle.seek_time=Settle.timeout;
			break;
		case 10: nmode=MODE_PROFILE_DOWN;    
			Down.Pmax=Steps.z4;
			break;
		case 11: nmode=MODE_SETTLE;   /* Settle 5 - bottom NOTE: time5 & z4  */
			Settle.timeout=Steps.time5;
			Settle.seek_time=Settle.timeout-2000;
			break;
		case 12: nmode=MODE_PROFILE_DOWN;   /* quick down to z5 */
			Down.Pmax=Steps.z5;
			break;
		case 13: /* insert to middle of mixed layer */
			Up.Pend=0.5*(Ballast.MLtop + Ballast.MLbottom);
			nmode=MODE_PROFILE_UP;  
			break;                     /* STORM MISSION ENDS HERE AT icall=30 */
		case 14: /* first drift */
			nmode=MODE_DRIFT_ML;
			break;
		case 15: /* Intermediate COMM */
			Drift.SetTarget=0;  /*Don't change Drift Target when Drift resumes */
			nmode=MODE_COMM;
			break;
		case 16: nmode=MODE_DRIFT_ML;  /* Second drift */
			break;
		default: nmode=MODE_ERROR;break;  /* can't get here */
	}
}

else if (stage==1 || stage==3 ) {  /* GAS FLOAT WAKE SEGMENT */
	if (icall==30 ) {
		log_event("START OF GAS WAKE SEGMENT \n");
		Ballast.MLtop=3;     /* Shallower ML */
		Ballast.MLbottom=12;
		Ballast.SetTarget=0;  /* don't set this automatically */
		Drift.timetype=2;  /* end at seconds of day */
		Drift.time_end_sec=73224;  /* 5am local at 130W,  5am - 8.66 hrs = 20.34Z */
		Steps.cycle=1;   /* Mostly turn off gas sensors and ANR */
		if (stage==3){   /* but not for optical float */
			Steps.cycle=0;
		}
	}
	switch ( (icall-30)%6) {
		case 0: /* insert to middle of mixed layer  */
			Up.Pend=0.5*(Ballast.MLtop + Ballast.MLbottom);
			nmode=MODE_PROFILE_UP;
			break;  
		case 1: nmode=MODE_DRIFT_ML;
			break;
		case 2: nmode=MODE_SETTLE;  /* Surface GTD measurement */
			Settle.SetTarget=0;  /* don't autoset */
			Ballast.Vset=0;  /* don't set Vol0 from surface */
			Settle.Target=PotDensity-10;  /* stay on surface */
			Settle.timeout=Steps.time0;
			Settle.seek_time=Settle.timeout;
			break;
		case 3: nmode=MODE_COMM;  /* COMM & Reset settle */
			Ballast.Vset=1;  /* Vol0 set if good settle */
			Settle.SetTarget=2;  /* Settle.Target = ballast.Target */
			Steps.Sig0=PotDensity;  /* Set Reference surface density */
			log_event("Surface Density %7.4f\n",Steps.Sig0-1000);
			if (icall<36){  /* first time */
				Ballast.Target=Steps.Sig0+Steps.dSig2;  /* ******** set target isopycnal */
				log_event("Ballast.Target %7.4f\n",Ballast.Target-1000);
			}
			break;
		case 4: nmode=MODE_PROFILE_DOWN;    /* Daily profile */
			Down.Pmax=Steps.z5;
			break;
		case 5: nmode=MODE_SETTLE;  
			Settle.timeout=Steps.time5;
			Settle.seek_time=Settle.timeout-2000;
			break;
		default: nmode=MODE_ERROR;
			log_event("icall=%d ERROR\n",icall);
			break;  /* can't get here */
		}
	}
else if (stage==2) {  /* VECTOR FLOAT WAKE SEGMENT */
	if (icall==30) {
		log_event("START OF VECTOR WAKE SEGMENT \n");
		/* Set up for isopycnal tracking */
		Ballast.MLthreshold=-10;  /* No DRIFT_ML or DRIFT_ISO */
		Ballast.MLmin=-10; 
		Ballast.MLbottom=-10;
		Ballast.SEEKthreshold=-10; /* YES DRIFT_SEEK */
		Drift.seek_Pmin=-10;
		Drift.seek_Pmax=300.;
		Ballast.Vset=0;   /* Don't recompute Vol0 - use Voff instead */
		Drift.VoffZero=0;  /* continuously set Voff - no reset at start of each */
		
		Drift.timetype=2;  /* end at seconds of day */
		Drift.time_end_sec=73224;  /* 5am local at 130W,  5am - 8.66 hrs = 20.34Z */
		
		Ballast.SetTarget=0;  /* Don't change target automatically*/
		Drift.SetTarget=2;    /* Drift target = Ballast.Target */
	}
	switch ( (icall-31)%5) {
		case -1: nmode=MODE_PROFILE_UP;  /* up to surface to get ML density */
			Up.Pend=7;
			break;
		case 0:  /* Down to isopycnal  */
			if (icall==31){
				Ballast.Target=PotDensity+Steps.dSig2;  /* set target */
				log_event("Target Isopycnal %7.4f\n",Ballast.Target-1000);
				Up.Pend=5.;
			}
			Down.Sigmax=Ballast.Target;
			Drift.Target=Ballast.Target;
			nmode=MODE_PROFILE_DOWN;
			break;  
		case 1: nmode=MODE_DRIFT_SEEK;   /* isopycnal drift */
			break;
		case 2: nmode=MODE_PROFILE_DOWN;  /* deep dive */
			Down.Pmax=Steps.z5;
			Down.Sigmax=5000;
			break;
		case 3: nmode=MODE_PROFILE_UP;
			break;
		case 4: nmode=MODE_COMM;  /* COMM  */
			break;
		default: nmode=MODE_ERROR;
			log_event("icall=%d ERROR\n",icall);
			break;  /* can't get here */
		}
	}
else {  /* Can't Get here unless stage set wrong */
	nmode=MODE_ERROR;
	log_event("STAGE ERROR 3 icall=%d stage=%d \n",icall,stage);
}

printf("%d\n",nmode);
++icall;
return(nmode);
}

#include "SampleTYPHOON10.c"

