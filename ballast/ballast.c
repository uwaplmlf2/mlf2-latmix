/*
 arch-tag: MLF2 CONTROL SOFTWARE
 Time-stamp: <2007-01-17 20:49:52 mike>

 mlf2_ballast.C Ballasting routine for DLF  - with Matlab interface
 usage:
 mlf2_ballast(day,P,T,S,T2,S2,B_in,mode_in,drogue_in,daysec,command,B,mode_out,drogue_out,telem_out)

 INPUT
 day - time in days since start of mission
 daysec - seconds of the current GMT day
 P - pressure in
 db (appropriate for chosen CTD combination)
 T - temperature in deg. C
 S- salinity in FU
 T2, S2 - second (upper) CTD
 command - integer command word
 B_in - target bocha position in m^3
 mode_in - mode at input, one of the MODE* constants in ballast.h
 drogue_in - drogue position (0=closed,1=open)

 OUTPUT
 B - ballaster position target in cubic meters
 mode_out - mode at output
 drogue_out - drogue position desired
 telem_out - output telemetry - to trackpoint in Intrusion floats

 This routine controls mode transitions
 and bocha motions for all modes
 but COMM & ERROR
 It does not move bocha, but returns B to calling routine.
 It does not move drogue, but returns drogue_out to calling
 routine

 Its actions are controlled by variables described in "ballast.h"
 It does not specify bocha motions during COMM
 Callingprogram should not call during COMM
 nor change the value of "mode" until all communications are
 finished.
 Values of Mode outside of valid values will trigger an error
 and return of mode=MODE_ERROR;

 7/14/03 - Start CBLAST03 Mods
 11/1/03 - Start ADV float mods
 2/1/04 - Additional ADV float mods - add Pycnocline mission
 2/27/04 - Add EOS mission
 6/8/04 - CBLAST04 Mods
 7/11/04 - Add GASTEST mission
 7/14/04 - Replace with different GASTEST mission
 7/28/04- final CBLAST04 changes, add HURRICANEGAS mission, add Creep
 8/21/04 - minor mods to PYCNOCLINE mission for equatorial expendable - variables changed
 9/06/04 - screening for zero CTD values added - PYCNOCLINE mission only
 1/29/05 - Put missions in separate files.  Use Include to put them in if necessary
 2/21/05 - South China mods
 4/28/06 - AESOP mods - considerable changes
 9/21/06 - DOGEE gas float changes
 5/30/07 - Intrusion acoustic command floats - extra output variable telemetry_out
 11/11/07 - NAB 
 6/23/08 - Hurricane 2008 - only mission changes
3/19/009 - Hurricane 2009 - added z2sigma routine
11/10/10 - Lake Washington - Add Depth error band - Drift and settle modes only
3/311      - LATMIX
9/12/11   -Lake Washington 2
 */
#include <math.h>
#include <stdio.h>
#include <string.h>
#include "ballast.h"    /*  This contains function & structure definitions */
#include "mtype.h"	/* Mission-type macros */
#ifndef SIMULATION
#include "ptable.h"
#include "config.h"
#endif

#ifdef SIMULATION
#define log_event printf

#define ballast_log printf
 
/* __________  next section only for Matlab use __________*/
#include "mex.h"

/* Input Arguments */

#define DAY_IN 		prhs[0]
#define P_IN            prhs[1]
#define T_IN		prhs[2]
#define S_IN            prhs[3]
#define T2_IN		prhs[4]
#define S2_IN            prhs[5]
#define B_IN		prhs[6]
#define M_IN            prhs[7]
#define D_IN            prhs[8]
#define DSEC_IN      prhs[9]
#define CMD_IN       prhs[10]

/* Output Arguments */

#define B_OUT   plhs[0]
#define M_OUT   plhs[1]
#define D_OUT	plhs[2]
#define T_OUT	plhs[3]

#else

#include "log.h"

void ballast_log(const char *fmt, ...);

#endif /* SIMULATION */

/* Initialize variables and structures in separate file */
/*DO THIS BELOW IN MISSION # include "Initialize.c" */

/*__________________________________________________________*/
/* Mode switching Routine  next_mode.c
Call this at the end of each mode
It determines which mode is next and changes parameters as appropriate

Different missions have different versions of this subroutine
Code is not here, but put in by include at compilation
*/

/* global variables: */
static double save1,save2,save3,STsave[5],ICsave[4],SDsave[13];
static int SDisave[7],isave;
static short Pgoalset=-1; /* 1 if Ballast.Target has been set */
int do_command;		/* act on this command if >0 */
static int command_end=0;         /* flag set by next_mode to command mode end */
static short commhome=1;      /* home at end of comm, 1=yes, 2=no */
static double PotDensity;	     /* global potential density */
static double PressureG;	     /* global Pressure */
static int commandG;		     /* global command */

/* Dummy function.  Do not remove */
void MFUNC
{
}

/* ----------------------- FLOAT MISSIONS  & PARAMETERS ---------------- */
/* Other mission files exist but not listed here */
# if defined LAKE2
# include "Missions/InitializeLAKE2.c"
# include "Missions/LAKE2.c"
#endif

# if defined LATMIX
# include "Missions/InitializeLATMIX.c"
# include "Missions/LATMIX.c"
#endif

# if defined LAKE
# include "Missions/InitializeLAKE.c"
# include "Missions/LAKE.c"
#endif

# if defined TYPHOON10
# include "Missions/InitializeTYPHOON10.c"
# include "Missions/TYPHOON10.c"
#endif

# if defined HURR09
# include "Missions/InitializeHURR09.c"
# include "Missions/HURR09.c"
#endif

# if defined PUGETSOUNDBALLASTEOS
# include "Missions/InitializePUGETSOUNDBALLASTEOS.c"
# include "Missions/PUGETSOUNDBALLASTEOS.c"
#endif


# if defined GASTEST
# include "Missions/InitializeGASTEST.c"
# include "Missions/GASTEST.c"
#endif


/*__________________________________________________________*/


/* Float ballasting routine starts here   */

/* This subroutine is the float ballasting subroutine to be
used in MLF2
It is driven by a Matlab simulation program,
or  used in the float directly
*/

/* Start set_ballast function  */
void
mlf2_ballast(double       day_time,
	     double       Pressure,
             double       Temperature,
	     double       Salinity,
	    double       Temperature_2,
	     double       Salinity_2,
	     double       ballast,
	     int          mode,
	     int          drogue,
             double	  daysec,
	    int    command,
	     double       *B,
	     int          *mode_out,
	     int          *drogue_out,
	     double   *telem_out)
{
    /* ballasting variables internal to this routine */
    static double mode_start=-999;      /* time this mode started, -999 if not
    started yet */

    static double last_P= -1000.;   /* Pressure  at last call */
    static double last_day_time= -1000.;
    static long  iout=0;   /* call counter - controls diagnostic output */
    static long isettle=0;  /* counts number of settle mode calls */
    static long idrift=0;    /* counts drift mode calls */
    static double last_profile_time= -1000;  /* time since last profile */
    static double vsave[Nav];     /* temporarily holds volume estimates for averaging */
    static double Tsave[5];          /* hold mixed layer T & S estimates for median filtering */
    static double Ssave[5];
    static double Rsave[5];       /* in situ density */
    static double Sigsave[5];     /* potential density */
    static double TemperatureP=0.;
    static double SalinityP=0.;
    static int Nquit=0.;    /* Counter for Down mode quit */
    static double surfacetime=0.;   /* time in UP mode since reaching UP.Pend */
    double mode_time;   /* time in this mode */
    static double Dsig0=0.;    /* save previous values of Dsig */
    static double Pstart=0;      /* save initial Pressure in a mode */
    static double Mass;		  /* Mass corrected for Creep */
    static short nbadctd=0;    /* number of sequential bad CTD values */
    static double Perrtimeref=0;  /* time reference for pressure band errors */
    static FILE *fout;		  /* ballast log */
    double time_step_sec;  /* sampling time step  computed from data */
    double Dsig=0;
    double rho,x,y,vol,PressureP,r,next_P,Plow,Phi,Pdev,Siglow;
    double Density,Potemp;		/* Precompute these */
    short Binit;
    int i,j,n;

    Pdev = 0.;
    
    /* printf("M%d",mode); */
    if(last_P== -1000. ){   /* first entrance */
	log_event("Ballasting start\n");
	/* ballast_log headers*/

	ballast_log(
"day_time,mode,mode_time,Settle.Target,Drift.Target,Ballast.Target,Ballast..V0,Ballast.Vdev,Mass, Bugs.weight\n"); 
	ballast_log("1,1,86400,1,1,1,1e6,1e6,1000,1000\n");  /* scale factors from MKS */
	log_event("Ballast log time mark %f\n",day_time);

	Drift.Voff=0;  /* Intially guess that Ballast values are OK */
	Mass=Mass0;
	*B=0;
	*telem_out=0.;
	last_P=Pressure;  last_day_time=day_time;
	mode_start=-999;
	last_profile_time=day_time;
	Perrtimeref=0.;

	/* INITIALIZATION CALL */
	*mode_out = next_mode(MODE_START,day_time);
	*drogue_out=drogue;

	memset(vsave, 0, sizeof(vsave));  /* zero out vsave */
	
	return;
    }   /* end first entrance */
	
/* Check for command -eliminate invalid  -  only act if do_command>0 */
if (command<1 || command>NCOMMAND ){ /* Null commands */
    do_command=0;
}
else{  
    log_event("COMMAND %d  received\n",command);
    do_command=command;
    mode=next_mode(mode,day_time);
}
commandG=command; /* Global command */

/* correct CTD values */
Pressure=Pressure + CTD.Poffset;  /* pressure at middle of float */
PressureG=Pressure;  /* Global Pressure */
if (Temperature!=0. && Salinity !=0.){
	Salinity=Salinity+CTD.BottomSoffset;
	Temperature=Temperature+CTD.BottomToffset;
}
if (Temperature_2!=0. && Salinity_2 !=0.){
	Salinity_2=Salinity_2+CTD.TopSoffset;
	Temperature_2=Temperature_2+CTD.TopToffset;
}

/* Choose CTD values for use in ballasting */
/* Default is bottom */
if (CTD.which==TOPCTD 
		&& Salinity_2 !=0 && Temperature_2 !=0.){
	if (Pressure > CTD.Ptopmin ){  
		Temperature=Temperature_2;
		Salinity=Salinity_2;
	}
	else{    /* In bubble zone, Top CTD is not good */
	 /* use previous good values*/
			Temperature=TemperatureP;
			Salinity=SalinityP;
	}
}
else if (CTD.which == MEANCTD
		&& Pressure > CTD.Ptopmin
		&& Salinity_2 !=0. && Temperature_2 !=0.
		&& Salinity !=0.     && Temperature !=0. ){
	Salinity=(Salinity+Salinity_2)/2.;
	Temperature=(Temperature+Temperature_2)/2.;
}
else if (CTD.which == MAXCTD && Salinity_2 > Salinity){ 
	Temperature=Temperature_2; 
	Salinity=Salinity_2;
}
else if (Salinity==0 && Temperature==0 && Pressure > CTD.Ptopmin
		&& Salinity_2 !=0. && Temperature_2 !=0.){
	Temperature=Temperature_2;  /* bottom is bad */
	Salinity=Salinity_2;
}
else {  /* Default */
	Temperature=Temperature;
	Salinity=Salinity;
}

if( Temperature==0. && Salinity==0.) {  /* bad ctd value */
	printf("Z");
	nbadctd=nbadctd+1;
	if (nbadctd>CTD.BadMax) {  /* too many bad CTDs in a row, ERROR*/
		mode=MODE_ERROR;
		log_event("Too many bad CTD\n");
		nbadctd=0;  /* reset in case it happens again */
	}
	else if (mode==MODE_COMM) { /* coming out of COM - go head and switch modes */
	}
	else {/* skip this call and hope that next CTD is OK */
		if(Pressure>bottom_P)
	                  ballast=ballast+deep_control*(Pressure-bottom_P); /* deep control */
		*mode_out=mode;
		*drogue_out=drogue;
		*B=ballast;
		return;  
	}
}
else { /* if not bad, then reset counter */
	nbadctd=0;  
}
TemperatureP=Temperature;
SalinityP=Salinity;

time_step_sec=(day_time-last_day_time)*86400.;
if (mode_start== -999){
    mode_start=day_time;
}
mode_time=day_time-mode_start;
/*if(fmod(day_time*24,1.0)<fmod(last_day_time*24,1.0))  /* what is this ??? */

/* if (iout % 10000==10) log_event("\ntime_step_sec %5.0f\n",time_step_sec); */

if (Pressure<0)
PressureP=0;  /* Positive Definite Pressure */
else
PressureP=Pressure;

/* precompute seawater properties */
Density=sw_dens(Salinity, Temperature,Pressure);
PotDensity=sw_pden(Salinity, Temperature,Pressure,0);
Potemp=sw_ptmp(Salinity,Temperature,Pressure,0.);
*telem_out=PotDensity;

if (PotDensity<RHOMIN || Density<RHOMIN ){  /* Avoid divide by zero */
mode=MODE_ERROR;
*mode_out=mode;
*drogue_out=drogue;
log_event("DENSITY ERROR1  %f  %f\n",Density,PotDensity);
return;
}

/* Modify sampling based on current information & mode */
sampling(mode, day_time, daysec);

Mass=Mass0+Creep*day_time;  /* Increase mass by Creep */

/* set BallastTarget from Ballast.Pgoal */
if (Ballast.SetTarget==8 &&
    ((Pressure-Ballast.Pgoal)*(last_P-Ballast.Pgoal)<0) ){
    Ballast.Target=PotDensity;
    Pgoalset=1;
    log_event("Set Ballast.Target %6.3f at %5.1fdbar \n",Ballast.Target-1000.,Pressure);
}

/* record data */
if (Mlb.record==1 && Mlb.point<Nsave ){
	Mlb.Psave[Mlb.point]=Pressure;
	Mlb.Sigsave[Mlb.point]=PotDensity;
	Mlb.point=Mlb.point+1;
}

/***  Start actions based on mode ***/
/*******************************************************/
if( mode == MODE_PROFILE_DOWN){   /* Down leg */
if (mode_time==0){
    Pstart=Pressure; /* save initial value */
    log_event("DOWN Start Pressure %6.1f\n",Pstart);
    ballast=Down.B0;
}

drogue=Down.drogue;

if(Pressure>Down.Pmax || command_end==1
   || PotDensity>Down.Sigmax
   || mode_time>=Down.timeout/86400.){  /* end DOWN*/
   if (Ballast.SetTarget==6){
	Ballast.Target=PotDensity;
	log_event("Target set %6.3f\n",PotDensity-1000);
   }
    mode=next_mode(mode,day_time);
    mode_start= -999;
}
else{  /* stay in DOWN mode */
    if (Down.Brate==0.)ballast=Down.B0;
    else {
		/* control speed relative to target ascent rate */
		x=Pstart+Down.Speed*mode_time*86400.;  /* target position */
		if(Pressure > x+Up.PHyst ){
		ballast=ballast+Down.Brate*time_step_sec; /* too slow */
		}
		else if(Pressure< x-Up.PHyst ){
		ballast=ballast-Down.Brate*time_step_sec; /* too fast */
		}
		if (ballast>Up.Ball) ballast=Up.Ball;
		else if (ballast<Down.B0)ballast=Down.B0;
	  }
  }
}

/*******************************************************/
else if (mode==MODE_SETTLE){  /* settle mode */

if (mode_time==0.) { /* First entrance */
n=0;
Perrtimeref=mode_time;
/* First entrance: SET SETTLE GOAL */
if (Settle.SetTarget==1){
    Settle.Target=PotDensity;
}
else if (Settle.SetTarget==2){
    Settle.Target=Ballast.Target;
}
else if (Settle.SetTarget==3){
    Settle.Target=Drift.Target;
}	

/* First entrance: estimate ballast point from volume and CTD */
/* Use reference S,Th; present pressure and hull vol */
vol=Ballast.V0
- Drift.Compress*Pressure*Ballast.V0
+ Drift.Thermal_exp*(Temperature-Drift.Tref)*Ballast.V0
+ Drift.Air*10./(10.+ Pressure);
Settle.B0=(Mass+Drift.Moff+Bugs.weight)/Density-vol+Drift.Voff;

ballast=Settle.B0;   /* get head start on settle ballast setting */
log_event(
	  "Start Settle P  %4.2f B %4.0f Target %6.4f Set %d Air %5.2f\n",
	  Pressure,Settle.B0*1.e6,Settle.Target-1000.,Settle.SetTarget,Drift.Air*1.e6);
isettle=0;
} /* end First Entrance */

/* Check for depth error during Settle mode */
if (Error.Modes==2 || Error.Modes==3){
	if ( Pressure < Error.Pmax && Pressure>Error.Pmin) { /* OK */
		Perrtimeref=mode_time;  /* reset clock */
		}
	if (mode_time-Perrtimeref > Error.timeout/86400.) { /* Too long outside of band  */
		mode=MODE_ERROR;
		log_event("ERROR: Float outside of [%4.1f %4.1f] longer than %4.0f sec\n",
			Error.Pmin,Error.Pmax,Error.timeout);
		Perrtimeref=mode_time;  /* reset in case it happens again */
		*mode_out=mode;
		*drogue_out=drogue;
		return;
	}
}
else {
		Perrtimeref=mode_time;  /* reset clock */
}

/* Seek Settle.rho only for first part of settle
Allow rate to decay thereafter */

Dsig=PotDensity - Settle.Target;

if (mode_time <= Settle.seek_time/86400.){
    Dsig0=Dsig;}
else {
    Dsig0=Dsig*exp(-(mode_time-Settle.seek_time/86400)/Settle.decay_time*86400.);
}

/* Add fake stratification for unstratified situations */
Dsig0=Dsig0 + (Pressure-Settle.Ptarget)*Settle.Nfake*Settle.Nfake*PotDensity/9.8;

Settle.B0=Settle.B0
+Dsig0/Settle.Target*Ballast.V0*time_step_sec/Settle.tau;  /* Seeking */

if (Settle.B0<Settle.Bmin)Settle.B0=Settle.Bmin;   /* Limit Seeking amplitude */
if (Settle.B0>Settle.Bmax) Settle.B0=Settle.Bmax;

ballast = Dsig0/Settle.Target*Ballast.V0*Settle.beta + Settle.B0; /*PseudoComp*/

if (mode_time<Settle.drogue_time/86400.){
drogue=1;  }        /* remain open for drogue_time  */
else {drogue=0.; }


/* get float volume */
vol=(Mass+Drift.Moff+Bugs.weight)/Density -ballast -
Drift.Air*10./(10.+PressureP);

vol=vol / (1.-Drift.Compress*Pressure
	 + Drift.Thermal_exp*(Temperature-Drift.Tref));
/* printf("C: %7.1f\n",vol*1.e6); */
if (Settle.nav>Nav){Settle.nav=Nav;} /* prevent over/underflow */
if (Settle.nav<1){Settle.nav=1;}
vsave[isettle%Settle.nav]=vol;  /* Save recent volume estimates */
++isettle;

/* END SETTLE */
if ( (mode_time>Settle.timeout/86400. && isettle>Settle.nav )
     || command_end==1 || Settle.nskip==0 || Settle.timeout <0. ){ 

if ( fabs(Dsig*Ballast.V0) < Settle.weight_error && isettle>Settle.nav) /* good settle ??*/
{
	vol=filtmean(vsave,Settle.nav);  /* average Nav values */
	x=0;  /* Compute Error */
	for (i=0;i<Settle.nav;++i)
	    x=x+pow(vol-vsave[i],2.);
	x=sqrt(x/Settle.nav);  /* Stdev of Volume estimates */
	Ballast.Vdev=x;
	if ( x<Settle.Vol_error) {   /* Settling OK  */
	    rho=PotDensity;
	    log_event("Settled P %4.2f  Sig0 %6.4f  dweight %4.1f Vol %7.1f (%5.2f) B %6.2f\n",
	       Pressure,rho-1000, Dsig*Ballast.V0*1000, vol*1e6,x*1.e6,ballast*1.e6);
	    if(Ballast.Vset==1 || Ballast.Vset==2){
		Ballast.V0=vol;
		log_event("Set Ballast.V0 %f\n",Ballast.V0*1e6);
	    }
	    if (Ballast.SetTarget==4){  /* set from end of Settle */
		Ballast.Target=PotDensity;
		log_event("Set Ballast.Target %6.3f\n",Ballast.Target-1000.);
	    }
	}
    else {   /* Settle NOT OK - do not use  */
	log_event("Not Settled: P %4.2f  Sig0 %6.4f  dweight %4.1f Vol %7.1f (%5.2f) B %6.2f\n",
	       Pressure,PotDensity-1000, Dsig*Ballast.V0*1000, vol*1e6,x*1.e6,ballast*1e6);
	}
}
else
{
    log_event("Settle not converged: P %4.2f Dsig %6.3f Dweight %4.1f\n",
	      Pressure,Dsig,Dsig*Ballast.V0*1000.);
}
mode=next_mode(mode,day_time);
mode_start= -999;

    }
}
/*******************************************************/
else if (mode==MODE_PROFILE_UP) {/* Up leg */
if (mode_time==0.){
    Pstart=Pressure; /* save initial value */
    log_event("Up Start Pressure %6.1f\n",Pstart);
    surfacetime=0.;  /* reset surface clock */
}

if (Pressure<Up.Pend && surfacetime==0.){   /* start surface clock */
	surfacetime=mode_time;  /* >0 means clock is running */
	/* printf("Start surface %6.1f %6.0f sec\n",Pressure,mode_time*86400.); */
}

    /* minimize descent if Speedbrake on */
if(Up.Speedbrake==1 && Pressure>Pstart ){
	ballast=Up.Ball;
	drogue=1;  /* speedbrake */
    }
else {
	drogue=Up.drogue;
	if (Up.Brate==0.)ballast=Up.Ball;
	else {
	  /* control speed relative to target ascent rate */
	  x=Pstart-Up.Speed*mode_time*86400.;  /* target position */
	  if(Pressure > x+Up.PHyst ){
	      ballast=ballast+Up.Brate*time_step_sec; /* too slow */
	    }
	  else if(Pressure< x-Up.PHyst ){
	      ballast=ballast-Up.Brate*time_step_sec; /* too fast */
	      }
	if (ballast>Up.Ball) ballast=Up.Ball;
	else if (ballast<0.)ballast=0.;
         }
}
	
if(    /* end UP*/
	( Up.surfacetime>0. && surfacetime>0.   /* surfacetime logic */
	    && mode_time-surfacetime >=Up.surfacetime/86400.)
	|| (Up.surfacetime<=0. && Pressure<Up.Pend)  /*if not; just making sure */
	|| mode_time >=Up.timeout/86400.
	|| command_end==1
   ){ 
  	/* printf("End UP %6.1f %6.0f sec  Dur %6.0f\n",
		Pressure,mode_time*86400.,(mode_time-surfacetime)*86400.); */
   
   if (Ballast.SetTarget==7){
	Ballast.Target=PotDensity;
	log_event("Target set %6.3f\n",PotDensity-1000.);
   }
    mode=next_mode(mode,day_time);
    mode_start= -999;
}
    
}
/*******************************************************/
else if(is_drift(mode) ) { /* Drift Modes  */
if (mode_time>Drift.closed_time/86400.)drogue=1;
else 	drogue=0;

if(mode_time==0.){  /* First entry */
	log_event("Ballast.V0 %f\n",Ballast.V0*1e6);
	Perrtimeref=mode_time;
	if (Drift.VoffZero==1){
		/* Assume Ballast.V0 is correct and float is neutral*/
		Drift.Voff=0.;
	}

	/* initialize Median filter with current values*/
	if(Drift.median==1){
		for (i=0;i<5;++i){
		Tsave[i]=Potemp;
		Ssave[i]=Salinity;
		Sigsave[i]=PotDensity;
		}
	idrift=0;
	}
}/* end first entry */

/* Check for depth error during Drift mode */
if (Error.Modes==1 || Error.Modes==3){
	if ( Pressure < Error.Pmax && Pressure>Error.Pmin) { /* OK */
		Perrtimeref=mode_time;  /* reset clock */
		}
	if (mode_time-Perrtimeref > Error.timeout/86400.) { /* Too long outside of band  */
		mode=MODE_ERROR;
		log_event("ERROR: Float outside of [%4.1f %4.1f] longer than %4.0f sec\n",
			Error.Pmin,Error.Pmax,Error.timeout);
		Perrtimeref=mode_time;  /* reset in case in happens again */
		*mode_out=mode;
		*drogue_out=drogue;
		return;
	}
}
else {
		Perrtimeref=mode_time;  /* reset clock */
}

/* GET DENSITY FOR BALLASTING */
if (Drift.median==1){  /* use 5 point median filter */
/* Update filter values */
    ++idrift;
    j=idrift%5;
    Tsave[j]=Potemp; /* Theta */
    Ssave[j]=Salinity;
    Sigsave[j]=PotDensity;
for (i=0;i<5;++i){ /* get saved density at current P */
	x=sw_temp(Ssave[i],Tsave[i],Pressure,0.); /* T at this P */   
	Rsave[i]=sw_dens(Ssave[i],x,Pressure);
}
Ballast.S0=opt_med5(Ssave);
Ballast.TH0=opt_med5(Tsave);
Ballast.P0=Pressure;
Ballast.rho0=opt_med5(Sigsave);
rho=opt_med5(Rsave); /* filtered rho at ML S,Th, Current P  */
}
else{  /* No filter */
    Ballast.S0=Salinity;
    Ballast.TH0=Potemp;
    Ballast.P0=Pressure;
    Ballast.rho0=PotDensity;
x=sw_temp(Ballast.S0,Ballast.TH0,Pressure,0.); /* temperature */
rho=sw_dens(Ballast.S0,x,Pressure);/* rho at ML S,Th;  current P*/
}

/* Use this density in ballasting equation */
if (rho<RHOMIN){  /* Avoid divide by zero */
mode=MODE_ERROR;
*mode_out=mode;
*drogue_out=drogue;
log_event("Density ERROR2 %f\n",rho);
return;
}

/* Filtered Pressure and density */
if (mode_time==0){
	/* Initialize Butterworth filters (in case they have changed) */
	/* note that ButterLow.Tfilt is master variable satellite setable */
	log_event("INITIALIZE FILTERS  Tfilt %4.0f",ButterLow.Tfilt);
	ButterLowCoeff(ButterLow.Tfilt, &ButterLow);
	ButterHiCoeff(ButterLow.Tfilt, &ButterHi);
	log_event("-> %4.0f\n",ButterLow.Tfilt);
	Binit=1;  /* reset at start of each drift */
}
else Binit=0;
/* Run filters */
/* Plow=Bfilt(&FiltPlow,Pressure,Pressure,Binit,ButterLow); /* Low Pass P */
Phi=Bfilt(&FiltPhi,Pressure,0.,Binit,ButterHi);        /* High Pass P */
Pdev=Bfilt(&FiltPdev,fabs(Phi),0.,Binit,ButterLow); /* Low Pass P deviations */
Siglow=Bfilt(&FiltSiglow,Ballast.rho0,Ballast.rho0,Binit,ButterLow); /* Low Pass Potential Density */

/* CHOOSE DRIFT MODE VARIANT */
if ( Pressure<Ballast.MLthreshold * Pdev  ||  Pressure < Ballast.MLmin
	||( Pressure>Ballast.MLtop && Pressure<Ballast.MLbottom )){
    mode=MODE_DRIFT_ML;     /* ML MODE */
}
else { /* ISO MODES */
	if(  Pressure > Drift.seek_Pmin   &&  Pressure <Drift.seek_Pmax  &&
               Pressure > Ballast.SEEKthreshold*Pdev){ /* with seek */
	   mode=MODE_DRIFT_SEEK;
	}
	else {    /* without seek */
		mode=MODE_DRIFT_ISO;
	}
}

#if 0
if (iout % 5 ==0)
printf("BBB   %6.4f  %5.1f  %5.1f %5.1f  %5.1f %d\n",day_time,Pressure,Plow,Phi,Pdev,mode);
#endif

/* SET & MODIFY TARGET DENSITY */
if (mode_time==0) {
	/* SET DRIFT GOAL */
	if (Drift.SetTarget==1){
		Drift.Target=PotDensity;
	}
	else if (Drift.SetTarget==2){
		Drift.Target=Ballast.Target;
	}
	else if (Drift.SetTarget==3){
		Drift.Target=Settle.Target;
	}
	log_event("Drift.Target Initialized %6.3f\n",Drift.Target-1000);
}
/* change when in ML */
if ( mode==MODE_DRIFT_ML ){
    if(Ballast.MLsigmafilt==1){
	Drift.Target=Siglow;
	}
    else {
	Drift.Target=Ballast.rho0;
	}
}

/* Pot. Density Anomaly - filtered or not */
Dsig=(Drift.Target-Ballast.rho0);
	
/* ISOPYCNAL SEEKING  */
/* Seek isopycnal with timescale Drift.isotime */
if( mode==MODE_DRIFT_SEEK){
    Drift.Voff=Drift.Voff -Dsig/Drift.iso_time*time_step_sec/rho*Ballast.V0;
    if (Drift.Voff<Drift.Voffmin)Drift.Voff=Drift.Voffmin;
    Dsig0=Dsig;  /* save isopycnal deviation (I'm not sure why) */
}

/* estimate average pressure in next time interval*/
if (mode_time>0) next_P=Pressure+(Pressure-last_P)/2;
else next_P=Pressure;
if (next_P<0) next_P=0;  /* could be fancier than this */

/* Compute bug Effect & flap drogue */
if (Bugs.stop_weight>0. && Bugs.start_weight>0.  && fabs(Bugs.start-Bugs.stop)>30.){
	if ( fabs(daysec-Bugs.start)<2*time_step_sec && daysec>=Bugs.start  && Bugs.weight==0 ){
		log_event("Sunset - bug compenstation ON %3.0f g\n",Bugs.start_weight*1000.);
		Bugs.weight=Bugs.start_weight;
	}
	if (Bugs.weight > 0.) {  /* nighttime and active */
		y=(Bugs.stop-Bugs.start);  /* duration of night/ seconds */
		if (y<0.)y=y+86400.;
		if (y<=0.){
			log_event("ERROR in Bugs start & stop %4.0f %4.0f\n",Bugs.start,Bugs.stop);
			Bugs.start= -1.e6;
			y=1e8;  /* Turn bugs off */
			}
		x=daysec-Bugs.start;
		if(x<0)x=x+86400.;
		if ( x<y){  /* continue night */
		    Bugs.weight=Bugs.start_weight+x/y*(Bugs.stop_weight-Bugs.start_weight);
		    if (fmod(x+Bugs.flap_duration,Bugs.flap_interval)<Bugs.flap_duration){
			drogue=0;  /* close drogue in flap */
			/*printf("F");*/
			}
		    }
		else{  /* dawn */
		    log_event("Sunrise (or error) - bug compenstation OFF\n");
		    Bugs.weight=0.;
		}
	}
}
    
/* BALLAST */
/* Use reference S,Th; present pressure and hull vol */
vol=Ballast.V0
- Drift.Compress*Pressure*Ballast.V0
+ Drift.Thermal_exp*(Temperature-Drift.Tref)*Ballast.V0
+ Drift.Air*10./(10.+ next_P);
    ballast=(Mass+Drift.Moff + Bugs.weight)/rho-vol+Drift.Voff+Ballast.Offset;

    /* Add Pseudo-compressibility  */
ballast=ballast -Drift.iso_Gamma*Dsig*Ballast.V0/rho;

/* END DRIFT  */
if( ( (Drift.timetype==1) /* time since end of last drift */
         && day_time-last_profile_time>Drift.time_end_sec/86400.)
 || ( (Drift.timetype==2) /* fraction of day in seconds 0-86400*/
         && fabs(daysec-Drift.time_end_sec)<2.*time_step_sec ) 
 || mode_time > Drift.timeout_sec/86400   /* duration of drift (always active) */
 || command_end==1 ){
    log_event("End drift: duration %5.0f sec Sod %5.0f\n",mode_time*86400,daysec);
    last_profile_time=day_time;
    mode=next_mode(mode,day_time);
    mode_start= -999;
    if (Ballast.SetTarget ==5){ /* set Ballast at end of Drift */
	Ballast.Target=PotDensity;
	log_event("Set Ballast.Target %6.3f\n",Ballast.Target-1000.);
    }
if (Ballast.Vset==2 || Ballast.Vset==3){
    Ballast.V0=(Mass+Drift.Moff+Bugs.weight)/Density -ballast
	- Drift.Air*10./(10.+Pressure);
    Ballast.V0=Ballast.V0/(1.-Drift.Compress*Pressure
			   + Drift.Thermal_exp*(Temperature-Drift.Tref));
    Drift.Voff=0;
    log_event("Set BallastV0 from Drift End %f \n",Ballast.V0*1.e6);
}
}
}/* end drift mode code */
/*******************************************************/

else if(mode==MODE_COMM) { /* Return from Comm mode */
	mode= next_mode(mode,day_time);
	mode_start= -999;
}
/*******************************************************/
else if (mode==MODE_DONE){
    log_event("DONE!\n");
    mode_start= -999;
}
else if (mode==MODE_ERROR){
  /* Just pass error along dont call next_mode()
   mode=next_mode(mode,day_time);
   mode_start=-999;
   */
}
else{
    log_event("Cant Get here - NonExistant Mode %d\n",mode);
    mode=MODE_ERROR;
    mode_start= -999;
}
/*******************************************************/

if (Pressure > error_P){
	log_event("Pressure emergency %3.0f\n",Pressure);
	mode=MODE_ERROR;
	mode_start=-999;
}
/* DEEP CONTROL */
if (Pressure > bottom_P){
    ballast=ballast+deep_control*(Pressure-bottom_P);
    printf("D");
}
/* SHALLOW CONTROL */
if (Pressure<top_P && ( mode==MODE_SETTLE || is_drift(mode) ) ){
        ballast=ballast-(top_P-Pressure)*shallow_control;
        };

if(iout%NLOG==0){
    log_event(
	      "%4.2f P %4.0f Ball %6.1f Mode %d Voff:%7.2f Target %6.3f Sig0 %6.3f \n",
	      day_time,Pressure,ballast*1.e6,mode,
              Drift.Voff*1.e6,Drift.Target-1000.,PotDensity-1000.);
}

/* write Ballast log information  */
#ifdef SIMULATION
if( ( iout %NLOG==1 || mode_start== -999) && day_time< LOGDAYSSIM ){
#else
if(  iout %NLOG==1 || mode_start== -999 ){
#endif 
ballast_log("%7.6f,%d,%7.6f,%7.4f,%7.4f,%7.4f,%7.1f,%5.2f,%7.1f,%7.1f\n",
day_time,mode,mode_time,Settle.Target-1000.,Drift.Target-1000.,Ballast.Target-1000.,
Ballast.V0*1e6,Ballast.Vdev*1e6,Mass*1000., Bugs.weight*1000.);
}
++iout;

    *mode_out=mode;
    *drogue_out=drogue;
    *B=ballast;
    last_P=Pressure;

    last_day_time=day_time;
}

/* returns mean of array X with maximum and minimum values removed */
double filtmean(double *X, int N)
{
    double	min,max,mean;
    int 	i;

    /* log_event("Filtering %d elements at 0x%08lx\n", N, (unsigned long)X);*/
    
    mean=0.;
    if (N<=2) return X[0];   /* Default return */
    min=1e10;
    max=-1e10;
    for (i=0;i<N;++i){
	/* log_event("X[%d] = %g\n", i, X[i]);*/
        if (X[i]<min)
	    min=X[i];
	else if (X[i]>max)
	    max=X[i];
	mean=mean+X[i];
    }
    mean=(mean-min-max)/(N-2);
    return mean;
}
/*%=========================================================================
*/
/* Second order Butterworth filter function */
/*                 this filter struc,  input data, initalize?, protoype filter */
double Bfilt(struct butter *B, double X, double YI, short init, struct butter P)
{
double Y;
  if (init==1){  /* initialize filter */
	  /* saved values all initialized to first value */
	  B->Xp=X;
	  B->Xpp=X;
	  B->Yp=YI;    /* output values to YI */
	  B->Ypp=YI;
	  /* coefficients set to prototype values 
	      This allows coefficients to be changed by satellite */
	  B->A2=P.A2;   B->A3=P.A3; 
	  B->B1=P.B1;     B->B2=P.B2;   B->B3=P.B3;
	  B->Tfilt=P.Tfilt;
	  log_event("COEFF:%5.0f %e %e %e %e %e\n",B->Tfilt,B->A2,B->A3, B->B1, B->B2,B->B3);
  }

  /* Evaluate filter */
  Y=B->B1*X  +   B->B2*B->Xp  +  B->B3*B->Xpp
	- B->A2*B->Yp   -   B->A3*B->Ypp;
  /* save values */
   B->Ypp=B->Yp;
   B->Yp=Y;
   B->Xpp=B->Xp;
   B->Xp=X;
   return Y;
}
/*------------------------------------------------------------------------------------ */
/* returns density of Mixed Layer Base from profile */
/* profile should be  downcast, ending in long monotonic section */
double getmlb( struct mlb * X)  /* X is pointer to structure */
{
	double Sigmlb,Pp,dP,Sigmin,Sigmax;
	short Pindex[Nsave],N,Np;
	int imax, i,j;
	/* Structure X:  *save are input data, shallow to deep
				*bin are computed equally spaced
				*/
	
	printf("getmlb start\n");
	imax=X->point-1;  /* maximum index */
	if (imax>Nsave-1){log_event("ERROR: getmlb Err 1 %d\n",imax);
					return -1;}
	
	/* make monotonic by removing points, bottom up */
	Pp=X->Psave[imax];
	j=imax;
	Pindex[imax]=imax;
	for (i=imax-1;i>=0;--i){    /* find indices of monotonic points */
		if (X->Psave[i]<Pp){  /* good */
			j--;
			if(j<0 || j>Nsave-1){log_event("ERROR: getmlb Err 2 %d\n",j);
						return -1;}
			Pindex[j]=i;
			Pp=X->Psave[i];
		}
	}
	for (i=imax;i>=j;--i){     /*delete nonmonotonic data and resave array */
		X->Psave[i]=X->Psave[Pindex[i]];
		X->Sigsave[i]=X->Sigsave[Pindex[i]];
	}
	Sigmin=2000;Sigmax=0;  /* also find min and max */
	for (i=0; i<(imax - Pindex[j]); ++i){
		if (i>Nsave-1){log_event("ERROR: getmlb Err 3 %d\n",i);return -1;}
	
		X->Psave[i]=X->Psave[i+ Pindex[j]];
		X->Sigsave[i]=X->Sigsave[i+ Pindex[j]];
		/* printf("%d  %f %f\n",i,X->Psave[i],X->Sigsave[i]);*/
		if (X->Sigsave[i] >Sigmax)Sigmax=X->Sigsave[i];
		if (X->Sigsave[i] <Sigmin)Sigmin=X->Sigsave[i];
	}
	imax=i;  /*new length */
	/* printf("min %f max %f\n",Sigmin,Sigmax);*/
	log_event("mlb raw data:%d points %5.1f  to %5.1fdb\n",imax,X->Psave[0],X->Psave[imax]);

	/* now grid onto uniform grid */
	for (i=0;i<Ngrid-1;++i){ /* fill grid */
		X->Pgrid[i]=i*X->dP;
		X->Siggrid[i]=-1.;  /* fill with bad flags */
	}
	j=0; /* data point index */
	for (i=0;i<Ngrid-1;++i){ /* for each grid point */
		/* move data to grid, check for overflow */
		while(X->Psave[j+1] < X->Pgrid[i] && j<imax-1 && j<=Nsave-2){++j;}
		if (j>=imax || j>Nsave-2){break;} /* EOD */
		if ( X->Psave[j] >= X->Pgrid[i] ){continue;} /* no data for this grid */
		
		if (X->Psave[j] < X->Pgrid[i] &&  X->Psave[j+1] >= X->Pgrid[i]){ /* if data */
			dP=(X->Psave[j+1]-X->Psave[j]);
			if (dP<=0) /* check for divide by zero */
				{log_event("ERROR: getmlb Err 4 %f\n",dP);return -1;}
			
			X->Siggrid[i]=X->Sigsave[j] +  /* interpolate to grid */
			 (X->Sigsave[j+1]-X->Sigsave[j])/dP*(X->Pgrid[i] - X->Psave[j]);
		}
	}
	/*
	for (i=0;i<Ngrid-1;++i){
		printf("%d  %f %f\n",i,X->Pgrid[i],X->Siggrid[i]);
	}*/
	/* count number of points, must be >Nmin */
	N=0;
	for(j=0;j<Ngrid;++j){
		if(X->Siggrid[j]>0)++N;
	}
	log_event("mlb gridded data:%d points %5.1f to %5.1fdb\n",N,X->Pgrid[0],X->Pgrid[N-1]);
	if (N<X->Nmin){log_event("ERROR: getmlb Err 5 %d\n",N);return -1;}
	
	/* Apply MLB algorithm */
	if (X->dSig==0){log_event("ERROR: getmlb Err 6 %f\n",X->dSig);return -1;}
	j=0; /* safety index */
	imax=2*fabs(Sigmax-Sigmin)/X->dSig;  /* max loops = twice estimated */
	
	Sigmlb=Sigmin - 3*X->dSig;
	N=0;Np=0;
	while( N>=Np && Sigmlb<Sigmax){
		++j;
		if(j>imax){log_event("ERROR: getmlb Err 7 %d\n",j);return -1.;}
		Sigmlb=Sigmlb + X->dSig;
		Np=N;
		N=0;
		for (i=0; i<Ngrid-1;++i){ /*get number of points */
			if( fabs( X->Siggrid[i]-Sigmlb) < X->dSig
			    && X->Siggrid[i]>0)++N;
		}
	}
	if (Sigmlb>=Sigmax || Sigmlb< Sigmin ){
	log_event("ERROR: getmlb Err 8 %f\n",Sigmlb);return -1;}
	
	return Sigmlb;
}

/*------------------------------------------------------------------------------------ */
/* returns potential density at Z from logged profile  */
/*    first data that spans the depth is used  */
/*  if this is not found, returns mean density  */
/*  if error, return -1 */
double z2sigma( struct mlb * X, double Z)  /* X is pointer to structure */
{
	double Pp,dP,Sigmean,x1,x0,S1,S0;
	short Pindex[Nsave],N,Np;
	int imax, i,j;
	/* Structure X:  *save are input data
				*/
	
	log_event("z2sigma seeking %6.3f\n",Z);
	imax=X->point-1;  /* maximum index */
	if (imax>Nsave-1 || imax<2 ){log_event("ERROR: z2sigma: bad length %d\n",imax);
					return -1;}
	Sigmean=0;
	for (i=1; i<imax ; ++i){
		if (i>Nsave-1){log_event("ERROR: z2sigma: index error %d\n",i);return -1;}
		x0=X->Psave[i-1];  /* two points near i */
		x1=X->Psave[i];
		S1=X->Sigsave[i];
		S0=X->Sigsave[i-1];		
		Sigmean=Sigmean-1000.+S1;
		if ( (x0>Z && Z>=x1) || (x1>Z && Z>=x0)) { /* found a point */
			if (x1==x0){
				return (S1+S0)/2.;
			}
			else{
				return S1+(S0-S1)/(x0-x1)*(Z-x1);
			}
		}
	}
	Sigmean=1000.+Sigmean/(imax-1);
	log_event("WARNING: z2sigma returning mean %6.3f\n",Sigmean);
	return(Sigmean);
}
					
/*------------------------------------------------------------------------------------ */


#ifdef SIMULATION
/* ___________________________cut here_________________________________*/
/*  MATLAB INTERFACE ROUTINE - IGNORE FOR FLOAT INSTALLATION */
void mexFunction(
		 int nlhs,       mxArray *plhs[],
		 int nrhs, const mxArray *prhs[]
		 )
{
    double
    *day,*P,*T,*S,*T2,*S2,*B_in,*mode_in,*drogue_in,*daysec0,*command_in;
    double
	*B,*mode_out,*drogue_out, *telem_out;
    int           i_mode_out,i_drogue_out;

    unsigned int  m,n;

    /* Check for proper number of arguments */

    if(nrhs != 11) {
	    mexErrMsgTxt("set_ballast requires 10 input arguments.");
    } else if (nlhs !=4) {
	    mexErrMsgTxt(
		  "set_ballast requires 4 output arguments.");
     }

    /*
     Create a matrix for the return argument */

    B_OUT =mxCreateDoubleMatrix(1, 1, mxREAL);
    M_OUT = mxCreateDoubleMatrix(1, 1,mxREAL);
    D_OUT = mxCreateDoubleMatrix(1, 1, mxREAL);
    T_OUT = mxCreateDoubleMatrix(1, 1, mxREAL);

    /* Assign pointers to the various parameters */

    B = mxGetPr(B_OUT);
    mode_out =mxGetPr(M_OUT);
    drogue_out = mxGetPr(D_OUT);
    telem_out = mxGetPr(T_OUT); 
    day = mxGetPr(DAY_IN);
    P = mxGetPr(P_IN);
    T = mxGetPr(T_IN);
    S = mxGetPr(S_IN);
    T2= mxGetPr(T2_IN);
    S2 = mxGetPr(S2_IN);
    B_in =mxGetPr(B_IN);
    mode_in = mxGetPr(M_IN);
    drogue_in= mxGetPr(D_IN);
    daysec0=mxGetPr(DSEC_IN);
    command_in=mxGetPr(CMD_IN);

    /* Do the actual computations in a subroutine */

    mlf2_ballast(*day,*P,*T,*S,*T2,*S2,*B_in,
	(int)*mode_in,(int)*drogue_in,*daysec0,(int)*command_in,
		 B,&i_mode_out,&i_drogue_out,telem_out); 
    *mode_out = i_mode_out;
    *drogue_out=i_drogue_out;

    return;
}
#endif /* SIMULATION */

#ifndef SIMULATION
#include <stdarg.h>
#include <stdlib.h>
#include <unistd.h>
#include "util.h"

/*
 * Log ballasting diagnostic data.
 */
void
ballast_log(const char *fmt, ...)
{
    va_list	args;
    static short bal_records = 0, bal_max_records = 1000;
    static short bal_file_index = 0;
    static char bal_filename[16];
    FILE	*ofp;
    
    va_start(args, fmt);

    if(bal_records > bal_max_records || bal_records == 0)
    {
	bal_file_index++;
	sprintf(bal_filename, "bal%05d.txt", bal_file_index);
	if(fileexists(bal_filename))
	    unlink(bal_filename);
	bal_records = 0;
    }

    bal_records++;
    
    if((ofp = fopen(bal_filename, "a")) != NULL)
    {
	vfprintf(ofp, fmt, args);
	fclose(ofp);
    }
    else
	log_error("mission",
		  "Cannot open ballasting file\n");
    va_end(args);
}


/*
 * Parameter table initialization.  See ptable.c for details on how
 * the parameter table works.
 */
#include <stdlib.h>
#include <string.h>
#include "ptable.h"

unsigned long Sensors[NR_REAL_MODES];
short Si[NR_REAL_MODES];

/*
 * Initialize the mode-control parameters.
 */
INITFUNC(init_mode_params)
{

    memset(Sensors, 0, sizeof(Sensors));

    Sensors[MODE_PROFILE_UP] =   PROF_SENSORS;
    Sensors[MODE_PROFILE_DOWN] = PROF_SENSORS;
    Sensors[MODE_SETTLE] = SETTLE_SENSORS;
    Sensors[MODE_DRIFT_ISO] = DRIFT_SENSORS;
    Sensors[MODE_DRIFT_ML] = DRIFT_SENSORS;
    Sensors[MODE_DRIFT_SEEK] = DRIFT_SENSORS;


    memset(Si, 0, sizeof(Si));


    /*
    ** Note that for the profiles, the "sampling interval" is now
    ** used to specify the maximum time allowed for ballast adjustment
    ** during each sample.
    */
    Si[MODE_PROFILE_UP] = 10;
    Si[MODE_PROFILE_DOWN] = 10;

    Si[MODE_SETTLE] = 30;
    Si[MODE_DRIFT_ISO] = 30;
    Si[MODE_DRIFT_ML] = 30;
    Si[MODE_DRIFT_SEEK] = 30;

    /*
     ** Ballast control parameters
     */
    add_param("down.Pmax",		PTYPE_DOUBLE, &Down.Pmax);
    add_param("down.timeout",		PTYPE_DOUBLE, &Down.timeout);
    add_param("down.Sigmax",		PTYPE_DOUBLE, &Down.Sigmax);
    add_param("down.B0",			PTYPE_DOUBLE, &Down.B0);
    add_param("down.Speed",		PTYPE_DOUBLE, &Down.Speed);
    add_param("down.Brate",		PTYPE_DOUBLE, &Down.Brate);
    add_param("down.drogue",		PTYPE_SHORT,  &Down.drogue);

    add_param("settle.timeout",		PTYPE_DOUBLE, &Settle.timeout);
    add_param("settle.seek_time",	PTYPE_DOUBLE, &Settle.seek_time);
    add_param("settle.decay_time",	PTYPE_DOUBLE, &Settle.decay_time);
    add_param("settle.nav",		PTYPE_SHORT, &Settle.nav);
    add_param("settle.drogue_time",	PTYPE_DOUBLE, &Settle.drogue_time);
    add_param("settle.beta",		PTYPE_DOUBLE, &Settle.beta);
    add_param("settle.tau",			PTYPE_DOUBLE, &Settle.tau);
    add_param("settle.weight_error",	PTYPE_DOUBLE, &Settle.weight_error);
    add_param("settle.Vol_error",	PTYPE_DOUBLE, &Settle.Vol_error);
    add_param("settle.Ptarget",	PTYPE_DOUBLE, &Settle.Ptarget);
    add_param("settle.Nfake",	PTYPE_DOUBLE, &Settle.Nfake);
	add_param("settle.nskip",		PTYPE_SHORT, &Settle.nskip);
    add_param("settle.SetTarget",	PTYPE_SHORT, &Settle.SetTarget);
    add_param("settle.Target",		PTYPE_DOUBLE, &Settle.Target);
    add_param("settle.B0",			PTYPE_DOUBLE, &Settle.B0);
    add_param("settle.Bmin",		PTYPE_DOUBLE, &Settle.Bmin);
    add_param("settle.Bmax",		PTYPE_DOUBLE, &Settle.Bmax);

    add_param("up.Ball",			PTYPE_DOUBLE, &Up.Ball);
    add_param("up.Speed",		PTYPE_DOUBLE, &Up.Speed);
    add_param("up.Brate",			PTYPE_DOUBLE, &Up.Brate);
    add_param("up.Pend",			PTYPE_DOUBLE, &Up.Pend);
    add_param("up.timeout",		PTYPE_DOUBLE, &Up.timeout);
    add_param("up.Speedbrake",	PTYPE_SHORT,  &Up.Speedbrake);
    add_param("up.PHyst",			PTYPE_DOUBLE, &Up.PHyst);
    add_param("up.drogue",		PTYPE_SHORT,  &Up.drogue);
    add_param("up.surfacetime",	PTYPE_DOUBLE, &Up.surfacetime);

    add_param("drift.SetTarget",		PTYPE_SHORT, &Drift.SetTarget);
    add_param("drift.VoffZero",		PTYPE_SHORT, &Drift.VoffZero);
    add_param("drift.median",		PTYPE_SHORT, &Drift.median);
    add_param("drift.timetype",		PTYPE_SHORT, &Drift.timetype);
    add_param("drift.time_end_sec",	PTYPE_DOUBLE, &Drift.time_end_sec);
    add_param("drift.timeout_sec",	PTYPE_DOUBLE, &Drift.timeout_sec);
    add_param("drift.Tref",			PTYPE_DOUBLE, &Drift.Tref);
    add_param("drift.Voff",		PTYPE_DOUBLE, &Drift.Voff);
    add_param("drift.Voffmin",		PTYPE_DOUBLE, &Drift.Voffmin);
    add_param("drift.Moff",		PTYPE_DOUBLE, &Drift.Moff);
    add_param("drift.Air",			PTYPE_DOUBLE, &Drift.Air);
    add_param("drift.Compress",	PTYPE_DOUBLE, &Drift.Compress);
    add_param("drift.Thermal_exp",   PTYPE_DOUBLE, &Drift.Thermal_exp);
    add_param("Drift.Target",		PTYPE_DOUBLE, &Drift.Target);
    add_param("driftiso_time",		PTYPE_DOUBLE, &Drift.iso_time);
    add_param("drift.seek_Pmin",	PTYPE_DOUBLE, &Drift.seek_Pmin);
    add_param("drift.seek_Pmax",       PTYPE_DOUBLE, &Drift.seek_Pmax);
    add_param("drift.iso_Gamma",	PTYPE_DOUBLE, &Drift.iso_Gamma);
    add_param("drift.time2",		PTYPE_DOUBLE, &Drift.time2);
    add_param("drift.closed_time",	PTYPE_DOUBLE, &Drift.closed_time);
	
    add_param("Ballast.SetTarget",	PTYPE_SHORT, &Ballast.SetTarget);
    add_param("ballast.Vset",		PTYPE_SHORT, &Ballast.Vset);
    add_param("ballast.MLsigmafilt",   PTYPE_SHORT, &Ballast.MLsigmafilt);
    add_param("ballast.MLthreshold",   PTYPE_DOUBLE, &Ballast.MLthreshold);
    add_param("ballast.MLmin",		PTYPE_DOUBLE, &Ballast.MLmin);
    add_param("ballast.MLtop",		PTYPE_DOUBLE, &Ballast.MLtop);
    add_param("ballast.MLbottom",	PTYPE_DOUBLE, &Ballast.MLbottom);
    add_param("ballast.SEEKthreshold",   PTYPE_DOUBLE, &Ballast.SEEKthreshold);
    add_param("ballast.Offset",		PTYPE_DOUBLE, &Ballast.Offset);
    add_param("ballast.T0",		PTYPE_DOUBLE, &Ballast.T0);
    add_param("ballast.S0",		PTYPE_DOUBLE, &Ballast.S0);
    add_param("ballast.P0",		PTYPE_DOUBLE, &Ballast.P0);
    add_param("ballast.rho0",		PTYPE_DOUBLE, &Ballast.rho0);
    add_param("ballast.B0",		PTYPE_DOUBLE, &Ballast.B0);
    add_param("ballast.V0",		PTYPE_DOUBLE, &Ballast.V0);
    add_param("ballast.TH0",		PTYPE_DOUBLE, &Ballast.TH0);
    add_param("ballast.Vdev",		PTYPE_DOUBLE, &Ballast.Vdev);
    add_param("ballast.Pgoal",		PTYPE_DOUBLE, &Ballast.Pgoal);
    add_param("ballast.Target",		PTYPE_DOUBLE, &Ballast.Target);
	
    add_param("mlb.go",		PTYPE_SHORT, &Mlb.go);
    add_param("mlb.Nmin",	PTYPE_SHORT, &Mlb.Nmin);
    add_param("mlb.dP",		PTYPE_DOUBLE, &Mlb.dP);
    add_param("mlb.dSig",		PTYPE_DOUBLE, &Mlb.dSig);
    add_param("mlb.Sigoff",	PTYPE_DOUBLE, &Mlb.Sigoff); 

    add_param("ctd.which",	PTYPE_SHORT, &CTD.which);
    add_param("ctd.BadMax",	PTYPE_SHORT, &CTD.BadMax);
    add_param("ctd.Ptopmin",	PTYPE_DOUBLE, &CTD.Ptopmin);
    add_param("ctd.Poffset",	PTYPE_DOUBLE, &CTD.Poffset);
    add_param("ctd.Separation",	PTYPE_DOUBLE, &CTD.Separation);
    add_param("ctd.TopSoffset",	PTYPE_DOUBLE, &CTD.TopSoffset);
    add_param("ctd.TopToffset",	PTYPE_DOUBLE, &CTD.TopToffset);
    add_param("ctd.BottomSoffset",	PTYPE_DOUBLE, &CTD.BottomSoffset);
    add_param("ctd.BottomToffset",	PTYPE_DOUBLE, &CTD.BottomToffset);
	
	add_param("steps.dSig1",   PTYPE_DOUBLE, &Steps.dSig1);
	add_param("steps.dSig2",   PTYPE_DOUBLE, &Steps.dSig2);
	add_param("steps.dSig3",   PTYPE_DOUBLE, &Steps.dSig3);
	add_param("steps.dSig4",   PTYPE_DOUBLE, &Steps.dSig4);
	add_param("steps.dSig5",   PTYPE_DOUBLE, &Steps.dSig5);
	
	add_param("steps.time0",   PTYPE_DOUBLE, &Steps.time0);
	add_param("steps.time1",   PTYPE_DOUBLE, &Steps.time1);
	add_param("steps.time2",   PTYPE_DOUBLE, &Steps.time2);
	add_param("steps.time3",   PTYPE_DOUBLE, &Steps.time3);
	add_param("steps.time4",   PTYPE_DOUBLE, &Steps.time4);
	add_param("steps.time5",   PTYPE_DOUBLE, &Steps.time5);

	add_param("steps.z0",   PTYPE_DOUBLE, &Steps.z0);
	add_param("steps.z1",   PTYPE_DOUBLE, &Steps.z1);
	add_param("steps.z2",   PTYPE_DOUBLE, &Steps.z2);
	add_param("steps.z3",   PTYPE_DOUBLE, &Steps.z3);
	add_param("steps.z4",   PTYPE_DOUBLE, &Steps.z4);
	add_param("steps.z5",   PTYPE_DOUBLE, &Steps.z5);
	add_param("steps.cycle",  PTYPE_SHORT, &Steps.cycle);
	
	add_param("error.Modes",PTYPE_SHORT, &Error.Modes);
	add_param("error.Pmin",	PTYPE_DOUBLE, &Error.Pmin);
    add_param("error.Pmax",	PTYPE_DOUBLE, &Error.Pmax);
    add_param("error.timeout",	PTYPE_DOUBLE, &Error.timeout);
  			
    add_param("Mass",		PTYPE_DOUBLE, &Mass0);
    add_param("Creep",		PTYPE_DOUBLE, &Creep);
    add_param("deep_control",   PTYPE_DOUBLE, &deep_control);
    add_param("bottom_P",       PTYPE_DOUBLE, &bottom_P);
    add_param("error_P",       PTYPE_DOUBLE, &error_P);
    add_param("stage",       	PTYPE_SHORT, &stage);
    add_param("newstage",       PTYPE_SHORT, &newstage);
    add_param("top_P",			PTYPE_DOUBLE, &top_P);
    add_param("shallow_control",	PTYPE_DOUBLE, &shallow_control);
    add_param("telem:step",	         PTYPE_DOUBLE, &telem_Step);
    add_param("commhome",		PTYPE_SHORT, &commhome);

/* Butterworth filter parameter - sets both hi and low */
    add_param("butterlow.tfilt",   PTYPE_DOUBLE, &ButterLow.Tfilt);	
  
/* Bug parameters */
    add_param("bugs.start",   PTYPE_DOUBLE, &Bugs.start);	
    add_param("bugs.stop",   PTYPE_DOUBLE, &Bugs.stop);	
    add_param("bugs.start_weight",   PTYPE_DOUBLE, &Bugs.start_weight);	
    add_param("bugs.stop_weight",   PTYPE_DOUBLE, &Bugs.stop_weight);	
    add_param("bugs.flap_interval",   PTYPE_DOUBLE, &Bugs.flap_interval);	
    add_param("bugs.flap_duration",   PTYPE_DOUBLE, &Bugs.flap_duration);	
 
 /* intervals and samping */
    add_param("up:sensors",     PTYPE_LONG, &Sensors[MODE_PROFILE_UP]);
    add_param("down:sensors",   PTYPE_LONG, &Sensors[MODE_PROFILE_DOWN]);
    add_param("driftiso:sensors",  PTYPE_LONG, &Sensors[MODE_DRIFT_ISO]);
    add_param("driftml:sensors",  PTYPE_LONG, &Sensors[MODE_DRIFT_ML]);
    add_param("driftseek:sensors",  PTYPE_LONG, &Sensors[MODE_DRIFT_SEEK]);
    add_param("settle:sensors", PTYPE_LONG, &Sensors[MODE_SETTLE]);
    add_param("up:si",          PTYPE_SHORT, &Si[MODE_PROFILE_UP]);
    add_param("down:si",        PTYPE_SHORT, &Si[MODE_PROFILE_DOWN]);
    add_param("driftiso:si",       PTYPE_SHORT, &Si[MODE_DRIFT_ISO]);
    add_param("driftml:si",       PTYPE_SHORT, &Si[MODE_DRIFT_ML]);
    add_param("driftseek:si",       PTYPE_SHORT, &Si[MODE_DRIFT_SEEK]);
    add_param("settle:si",      PTYPE_SHORT, &Si[MODE_SETTLE]);

}


#endif /* ! SIMULATION */





