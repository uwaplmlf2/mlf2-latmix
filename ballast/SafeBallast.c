/*
 arch-tag: MLF2 CONTROL SOFTWARE
 Time-stamp: <2007-01-17 20:49:52 mike>

 safe_ballast.C Ballasting routine for DLF  - with Matlab interface
 usage:
 safe_ballast(day,P,T,B_in,mode_in,B,mode_out,drogue_out)

 INPUT
 day - time in days since start of mission
 daysec - seconds of the current GMT day
 P - pressure in db
 B_in - target bocha position in m^3
 mode_in - mode at input, one of the MODE* constants in ballast.h

 OUTPUT
 B - ballaster position target in cubic meters
 mode_out - mode at output
 drogue_out - drogue position desired

3/17/09  - SAFE BALLASTING VERSION
 */
#include <math.h>
#include <stdio.h>
#include <string.h>
#include "ballast.h"    /*  This contains function & structure definitions */
#include "mtype.h"	/* Mission-type macros */
#ifndef SIMULATION
#include "ptable.h"
#include "config.h"
#endif

#ifdef SIMULATION
#define log_event printf

#define ballast_log printf
 
/* __________  next section only for Matlab use __________*/
#include "mex.h"

/* Input Arguments */

#define DAY_IN 		prhs[0]
#define P_IN            prhs[1]
#define B_IN		prhs[2]
#define M_IN            prhs[3]

/* Output Arguments */

#define B_OUT   plhs[0]
#define M_OUT   plhs[1]
#define D_OUT	plhs[2]

#else

#include "log.h"

void ballast_log(const char *fmt, ...);

#endif /* SIMULATION */

/* Initialize variables and structures in separate file */
/*DO THIS BELOW IN MISSION # include "Initialize.c" */

/*__________________________________________________________*/

/* Dummy function.  Do not remove */
void MFUNC
{
}

/*__________________________________________________________*/


/* Float ballasting routine starts here   */

/* This subroutine is the float ballasting subroutine to be
used in MLF2
It is driven by a Matlab simulation program,
or  used in the float directly
*/

static struct safe Safe = {
   120.,   /* Ptarget - target depth /m */
   15.,     /* Pband deadband width +- about this /m */
   150.e-6, /* ballast0 first guess of ballast position at Ptarget */
   1.e-6,     /* dbdp  stabilizing slope - m^3 / db */
   1.16e-10,  /*Alpha=100e-6/86400./10.   PART 2- Seek Gain   m^3/sec/dbar */
   86400.,     /*timeout_sec   cycle time - surface and COMM this often */
   300.	   /* downtime  Initial time in down mode */
};

static double bottom_P=200;  /* Deep control as before */
static double deep_control = 25.e-6;
static double error_P=240.; /* Depth to declare emergency */

/* Start set_ballast function  */
void
safe_ballast(double       day_time,
	     double       Pressure,
	     double       ballast,
	     int          mode,
	     double       *B,
	     int          *mode_out,
	     int          *drogue_out)
{
   
   static double t_prev=0;
   static double t_start_days=0.;
   static int iout=0;
   double dp,dtsec,r,drogue;
      
   if (t_prev==0 || mode==MODE_COMM){  /* startup */
	ballast=0;
	dp=0.;
	t_start_days=day_time;
   }
   else{
         dtsec=(day_time-t_prev)*86400; /* time interval */
	dp=Pressure-Safe.Ptarget;
	dp=dp*(1.-exp(-pow((dp/2/Safe.Pband),2.) ));

	Safe.ballast0=Safe.ballast0+Safe.Alpha*dp*dtsec;   /* Seek  */
	if (Safe.ballast0<0){
		Safe.ballast0=0;
	}
	ballast=Safe.ballast0+Safe.dbdp*dp; /* gradient hold */
   }
   
   if(Pressure>bottom_P){
	ballast=ballast+deep_control*(Pressure-bottom_P); /* deep control */
	drogue=1;
	}
   else{
	drogue=0;
  }

  iout=iout+1;
  if (iout%100==1){
	log_event("%6.3f  P %4.1f  dP %4.1f  Ball %4.1f  Ball0  %4.1f\n"
				,day_time,Pressure,dp,ballast*1e6,Safe.ballast0*1e6);
  }
   
   if (  (day_time-t_start_days)*86400. > Safe.timeout_sec ){
	mode=MODE_COMM;
	log_event("Surface to COMM\n");
   }
   else if (  (day_time-t_start_days)*86400. < Safe.downtime ){
	mode=MODE_PROFILE_DOWN;
   }
   else if (Pressure > error_P){
	log_event("Pressure emergency %3.0f\n",Pressure);
	mode=MODE_ERROR;
   }
   else {
	mode=MODE_SETTLE;
   }
   
     t_prev=day_time;
    *mode_out=mode;
    *drogue_out=drogue;
    *B=ballast;
}


#ifdef SIMULATION
/* ___________________________cut here_________________________________*/
/*  MATLAB INTERFACE ROUTINE - IGNORE FOR FLOAT INSTALLATION */
void mexFunction(
		 int nlhs,       mxArray *plhs[],
		 int nrhs, const mxArray *prhs[]
		 )
{
    double
    *day,*P,*B_in,*mode_in;
    double
	*B,*mode_out,*drogue_out;
    int           i_mode_out,i_drogue_out;

    unsigned int  m,n;

    /* Check for proper number of arguments */

    if(nrhs != 4) {
	    mexErrMsgTxt("set_ballast requires 4 input arguments.");
    } else if (nlhs !=3) {
	    mexErrMsgTxt(
		  "set_ballast requires 3 output arguments.");
     }

    /*
     Create a matrix for the return argument */

    B_OUT =mxCreateDoubleMatrix(1, 1, mxREAL);
    M_OUT = mxCreateDoubleMatrix(1, 1,mxREAL);
    D_OUT = mxCreateDoubleMatrix(1, 1, mxREAL);

    /* Assign pointers to the various parameters */

    B = mxGetPr(B_OUT);
    mode_out =mxGetPr(M_OUT);
    drogue_out = mxGetPr(D_OUT);
    day = mxGetPr(DAY_IN);
    P = mxGetPr(P_IN);
    B_in =mxGetPr(B_IN);
    mode_in = mxGetPr(M_IN);

    /* Do the actual computations in a subroutine */

    safe_ballast(*day,*P,*B_in,
	(int)*mode_in,B,&i_mode_out,&i_drogue_out); 
    *mode_out = i_mode_out;
    *drogue_out=i_drogue_out;

    return;
}
#endif /* SIMULATION */

#ifndef SIMULATION
#include <stdarg.h>
#include <stdlib.h>
#include <unistd.h>
#include "util.h"

/*
 * Log ballasting diagnostic data.
 */
void
ballast_log(const char *fmt, ...)
{
    va_list	args;
    static short bal_records = 0, bal_max_records = 1000;
    static short bal_file_index = 0;
    static char bal_filename[16];
    FILE	*ofp;
    
    va_start(args, fmt);

    if(bal_records > bal_max_records || bal_records == 0)
    {
	bal_file_index++;
	sprintf(bal_filename, "bal%05d.txt", bal_file_index);
	if(fileexists(bal_filename))
	    unlink(bal_filename);
	bal_records = 0;
    }

    bal_records++;
    
    if((ofp = fopen(bal_filename, "a")) != NULL)
    {
	vfprintf(ofp, fmt, args);
	fclose(ofp);
    }
    else
	log_error("mission",
		  "Cannot open ballasting file\n");
    va_end(args);
}


/*
 * Parameter table initialization.  See ptable.c for details on how
 * the parameter table works.
 */
#include <stdlib.h>
#include <string.h>
#include "ptable.h"

unsigned long Sensors[NR_REAL_MODES];
short Si[NR_REAL_MODES];

/*
 * Initialize the mode-control parameters.
 */
INITFUNC(init_mode_params)
{

    memset(Sensors, 0, sizeof(Sensors));

    Sensors[MODE_PROFILE_UP] =   PROF_SENSORS;
    Sensors[MODE_PROFILE_DOWN] = PROF_SENSORS;
    Sensors[MODE_SETTLE] = SETTLE_SENSORS;
    Sensors[MODE_DRIFT_ISO] = DRIFT_SENSORS;
    Sensors[MODE_DRIFT_ML] = DRIFT_SENSORS;
    Sensors[MODE_DRIFT_SEEK] = DRIFT_SENSORS;


    memset(Si, 0, sizeof(Si));


    /*
    ** Note that for the profiles, the "sampling interval" is now
    ** used to specify the maximum time allowed for ballast adjustment
    ** during each sample.
    */
    Si[MODE_PROFILE_UP] = 10;
    Si[MODE_PROFILE_DOWN] = 10;

    Si[MODE_SETTLE] = 60;
    Si[MODE_DRIFT_ISO] = 60;
    Si[MODE_DRIFT_ML] = 60;
    Si[MODE_DRIFT_SEEK] = 60;

    /*
     ** Ballast control parameters
     */

    add_param("safe.Ptarget",		PTYPE_DOUBLE, &Safe.Ptarget);
    add_param("safe.Pband",		PTYPE_DOUBLE, &Safe.Pband);
    add_param("safe.ballast0",		PTYPE_DOUBLE, &Safe.ballast0);
    add_param("safe.dbdp",		PTYPE_DOUBLE, &Safe.dbdp);
    add_param("safe.Alpha",		PTYPE_DOUBLE, &Safe.Alpha);
    add_param("bottom_P",		PTYPE_DOUBLE, &bottom_P);
    add_param("deep_control",		PTYPE_DOUBLE, &deep_control);
    add_param("error_P",			PTYPE_DOUBLE, &error_P);
    add_param("safe.timeout_sec",	PTYPE_DOUBLE, &Safe.timeout_sec);


 /* intervals and samping */
    add_param("settle:sensors", PTYPE_LONG, &Sensors[MODE_SETTLE]);
    add_param("settle:si",      PTYPE_SHORT, &Si[MODE_SETTLE]);

}


#endif /* ! SIMULATION */





