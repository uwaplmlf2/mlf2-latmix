#
# arch-tag: rules for using Aztec C compiler under DOS (yuck)
#

.SUFFIXES: .lib .rhx .ahx .run .app .asm .a

# C source --> object code
.c.r:
	$(CC) $(CFLAGS) $<

.c.a:
	$(CC) $(CFLAGS) -a $<

.asm.r:
	$(AS) $(ASFLAGS) $<

# Linked executable --> hex
.$(BIN).$(HEX):
	srec68 $(SRFLAGS) $*.$(BIN)
	@copy $*.m00 $*.$(HEX)
	@erase $*.m00

clean:
	del *.r
	del *.run
	del *.app
	del *.rhx
	del *.ahx

-include $(DEP_FILES)
