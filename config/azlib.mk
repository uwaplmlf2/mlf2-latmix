#
# arch-tag: makefile component to build library archives (DOS version)
#

ifndef mklib
mklib := 1

AZOBJS = $(LIBOBJECTS:.r=.o)
AZLIB = $(THISLIB:.a=.lib)

all: $(AZLIB)

tmp.bld: $(AZOBJS)
	@erase tmp.bld
	$(foreach f,$(AZOBJS),$(shell echo $f >> tmp.bld)

$(AZLIB): $(AZOBJS) tmp.bld
	del $(THISLIB)
	ord68 tmp.bld tmp2.bld
	$(AR) $(AZLIB) -f tmp2.bld
	@erase tmp2.bld

endif
