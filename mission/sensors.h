/*
** $Id: sensors.h,v 1d7dab377860 2008/09/16 22:33:10 mikek $
*/
#ifndef _SENSORS_H_
#define _SENSORS_H_

#include "hash.h"
#include "adcp.h"
#include "netcdf.h"
#include "eco.h"
#include "gps.h"

#define MAX_PR_SAMPLES	60
#define MAX_PR_SENSORS	3

/* Pressure sensor channel indicies */
#define PR_INDEX_TOP		0
#define PR_INDEX_HULL		1
#define PR_INDEX_BOTTOM		2

struct sample_t {
    long		mode;
    unsigned long	sensors;	/**> sensor bitmask */
    GPSdata		*gdp;
    int			which_pr;	/**> which pressure sensor to use */
    int			ctds;		/**> count of CTDs */
    float		P[MAX_PR_SENSORS];		/**> pressure in dbars */
    float		T[2];		/**> temperature in degC */
    float		S[2];		/**> salinity is psu */
    float		gtd;		/**> GTD pressure in psi */
    float		therm;		/**> thermistor temp in degC */
    float		Pavg[MAX_PR_SENSORS];	/**> avg of fastP in dbars */
    float		rh;		/**> relative humidity */
    float		v[2];		/**> battery voltage */
    float		ipr;		/**> internal pressure */
    long		oxy;		/**> SB oxygen in hz */
    long		par;		/**> PAR sensor reading in mV */
    long		i490;           /**> i490 sensor reading in mV */
    long		npr;        /**> count of fastP samples */
    long		pr_tlast;        /**> time of last fastP sample (secs) */
    float		fastP[MAX_PR_SENSORS][MAX_PR_SAMPLES];
};

#define OFFSETOF(s,m) (size_t)&(((s *)0)->m)
#define SAMPLE_START	OFFSETOF(struct sample_t, ctds)

#define SAMPLE_SIZE (sizeof(struct sample_t) - SAMPLE_START)
#define CLEAR_SAMPLE(s) (memset(((char*)s)+SAMPLE_START, 0, SAMPLE_SIZE))

/* Data file type codes */
#define ENV_FILE	0
#define GPS_FILE	1
#define FASTPR_FILE	2

/* Data file manifest file */
#define DATAFILE_LIST	"filelist.txt"

/* Pressure difference (dbars) between CTDs and pressure sensor */
#define TOP_CTD_DELTA_PR	-0.584
#define BOT_CTD_DELTA_PR	0.838

void sens_get_err_counters(HashTable *ctable);
unsigned long sens_initialize_sensors(unsigned long which);
void sens_shutdown_sensors(unsigned long which);
long sens_read_env_data(unsigned long sensors, struct sample_t *s);
int sens_open_data_file(int type, void (*store)(const char *filename));
void sens_close_file(int type);
void sens_write_env_data(long t, float x, struct sample_t *s, int ql_flag);
void sens_write_noise_data(void);
int sens_read_store_adcp(int ftype, long t0, long dt, int nr_samples);
int __sens_sample_wave_spectra(FILE *fp, netCDFdesc *nd, int nr_samples, 
			   unsigned long dt_usecs, unsigned sensors);
int sens_read_store_wave(int nr_samples, unsigned dt);
void sens_check_all(void);
int sens_send_cmd(unsigned long which, const char *cmd);
void sens_write_gps_fix(GPSdata *gdp);
int sens_wait_for_gps(GPSdata *gdp, long timeout, int setclock, int n_fixes);
void sens_adcp_pulse_setup(int mode);
void sens_set_file_time(int type, long t);
int sens_maybe_close_file(int type);
int sens_open_sexp_files(int ql, FILE **datafp, FILE **qlfp, FILE **prfp);
int sens_read_pressure(struct sample_t *s, long *timestamp);
void adjust_pressure(double pr, int which, double *top_pr, double *bottom_pr);

#endif /* _SENSORS_H_ */
