/*
** $Id: sample.h,v 30ef1374b225 2007/04/15 03:14:25 mikek $
*/
#ifndef _SAMPLE_H_
#define _SAMPLE_H_

#include "hash.h"
#include "netcdf.h"


/*
** Counters to record the total number of samples and sensor
** errors.
*/
#define COUNTER_DECL(name)	unsigned long name ## _total, name ## _errs
#define INC_TOTAL(s,name)	s. ## name ## _total++
#define INC_ERR(s,name)		s. ## name ## _errs++
#define TOTAL(s,name)		s. ## name ## _total
#define ERRS(s,name)		s. ## name ## _errs

struct scounter_t {
    COUNTER_DECL(ctd);
    COUNTER_DECL(adcp);
    COUNTER_DECL(inertial);
    COUNTER_DECL(alt);
    COUNTER_DECL(par);
    COUNTER_DECL(flr);
};



/* Maximum length of "extra" message to send in COMM mode */
#define EXTRA_MESSAGE_LEN	128

void set_extra_message(const char *msg);
void mlf2_main_loop(int first_mode, long duration);

#endif /* _SAMPLE_H_ */
