/*
** $Id: message.h,v 30ef1374b225 2007/04/15 03:14:25 mikek $
*/
#ifndef _MESSAGE_H_
#define _MESSAGE_H_

#define XML_TEXT_FILE	"xmltext"

typedef void (*pi_handler)(const char *tag, const char *data);

int msg_init_parser(void);
void msg_add_handler(const char *tag, pi_handler handler);
int msg_parse_message(char *msg, int msglen, char *reply, int rlen, 
		      int allow_dup);
int msg_parse_file(FILE *ifp, FILE *ofp, int allow_dup);

#endif /* _MESSAGE_H_ */
