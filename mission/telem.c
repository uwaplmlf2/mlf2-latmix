/*
** Data telemetry functions using the ORE BART board.
**
*/
#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/fcntl.h>
#include <string.h>
#include <tt8lib.h>
#include <tt8.h>
#include <tpu332.h>
#include <qsm332.h>
#include <sim332.h>
#include <picodcf8.h>
#include "ptable.h"
#include "log.h"
#include "analogout.h"
#include "ioports.h"
#include "telem.h"

#define RELEASE_1_BIT		(1 << 6)
#define RELEASE_2_BIT		(1 << 7)
#define RELEASE_MASK		(RELEASE_1_BIT|RELEASE_2_BIT)

#define CLEAR_RELEASE1()	iop_clear(IO_F, RELEASE_1_BIT)
#define CLEAR_RELEASE2()	iop_clear(IO_F, RELEASE_2_BIT)

/** Release lines are active high */
#define RELEASE_1(p)		(((p) & RELEASE_1_BIT))
#define RELEASE_2(p)		(((p) & RELEASE_2_BIT))

#define DELAY_ZERO		100
#define DELAY_FULL_SCALE	1000

static unsigned	saved_pstate = RELEASE_MASK;

static long	telem_zero_delay = DELAY_ZERO;
static long	telem_full_scale_delay = DELAY_FULL_SCALE;

/*
 * Convert a pulse delay in milliseconds to the corresponding
 * analog output voltage.
 *
 *     volts = c0 + c1*delay
 *
 *       c0 = 0.66667 volts
 *       c1 = 0.00334 volts/ms
 */
static __inline__ float delay_to_volts(float ms)
{
    if(ms < DELAY_ZERO)
	ms = DELAY_ZERO;
    else if(ms > DELAY_FULL_SCALE)
	ms = DELAY_FULL_SCALE;
    return 0.66667 + 0.00334*ms;
}

static __inline__ short volts_to_counts(float v)
{
    return (short)(v*4095./5.);
}

void
telem_set_sense(int which)
{
    switch(which)
    {
	case 1:
	    iop_set(IO_F, RELEASE_1_BIT);
	    break;
	case 2:
	    iop_set(IO_F, RELEASE_2_BIT);
	    break;
	default:
	    break;
    }
}

void
telem_clear_sense(int which)
{
    switch(which)
    {
	case 1:
	    iop_clear(IO_F, RELEASE_1_BIT);
	    break;
	case 2:
	    iop_clear(IO_F, RELEASE_2_BIT);
	    break;
	default:
	    break;
    }

}


#ifndef TEST_TELEM
INITFUNC(init_telem_params)
{
    add_param("telem:full_scale_delay",	PTYPE_LONG|PTYPE_READ_ONLY,
	      &telem_full_scale_delay);
    add_param("telem:zero_delay",	PTYPE_LONG|PTYPE_READ_ONLY,
	      &telem_zero_delay);
}
#endif

/**
 * Transmit the supplied scaled value by encoding it in the inter-pulse
 * delay of the transmitter.
 *
 *    delay = zero_delay + value*(full_scale_delay - zero_delay)
 *
 * The units of 'delay' are milliseconds.
 *
 * @param  value  value on the range (0, 1) to transmit.
 */
void
telem_send_value(double value)
{
    float	delay, volts;

    if(value >= 0. && value <= 1.)
    {
	delay = telem_zero_delay + value*(telem_full_scale_delay - telem_zero_delay);
	volts = delay_to_volts(delay);
	log_event("Outputting %5.4fv on D/A\n", volts);
	da_output(volts_to_counts(volts));
    }
    else
	log_error("telem", "Value out of range: %f\n", value);
    
}

/**
 * Return the telemetered command from the surface.
 *
 * @return command code.
 */
int
telem_get_command(void)
{
    int		minute, cmd;
    unsigned	pstate;

    cmd = TELEM_CMD_NULL;
    pstate = iop_read(IO_A_REAL) & RELEASE_MASK;

    log_event("RELEASE state = 0x%02x (prev = 0x%02x)\n", pstate, saved_pstate);
    
    /* A command is only returned if the state has changed */
    if(saved_pstate != pstate)
    {
	/*
	 * We need to check that the release lines have changed from the
	 * unasserted to asserted state, i.e. we are looking for an edge.
	 */
	if(!RELEASE_1(saved_pstate) && RELEASE_1(pstate))
	{
	    log_event("RELEASE-1 asserted\n");
	    cmd = TELEM_CMD_SURFACE;
	}
	else if(!RELEASE_2(saved_pstate) && RELEASE_2(pstate))
	{
	    log_event("RELEASE-2 asserted\n");
	    
	    /* Calculate the minute of the hour */
	    minute = (RtcToCtm() % 3600)/60;

	    if(minute >= 5 && minute <= 25)
		cmd = TELEM_CMD_DOWN;
	    else if(minute >= 35 && minute <= 55)
		cmd = TELEM_CMD_UP;
	}
    }
    else
	log_event("RELEASE state unchanged\n");
    
    saved_pstate = pstate;
    return cmd;
}

#ifdef TEST_TELEM
#include <ctype.h>
static char line[80];

static const char *telem_help =
"At the TELEM prompt, enter a floating point value between 0 and 1\n"
"This value will be used to produce a inter-pulse delay of between\n"
"100 and 1000 milliseconds\n";

int
main(int ac, char *av[])
{
    double	offset, scale, span, value;
    char	*ptr;
    int		n;
    unsigned	pstate;
    
    /*
    ** Hardware initialization
    */
    InitTT8(NO_WATCHDOG, TT8_TPU);
    InitCF8(CF8StdCS, CF8StdAddr);
    if(errno != 0)
	printf("\nWARNING: InitCF8 failed, error %d\n", errno);
    if(errno == -1 || errno == PiDosNoHardware)
	goto done;
    
    SimSetFSys(4000000L);
    SerSetBaud(9600L, 0);

    init_ioports();
    
    fputs(telem_help, stdout);
    telem_clear_sense(1);
    telem_clear_sense(2);
    
    while(1)
    {
	printf("TELEM> ");
	fflush(stdout);
	if(!InputLine(line, (short)sizeof(line)-1))
	    continue;

	ptr = line;
	
	/* Eat leading white space */
	for(ptr = line;isspace(*ptr);ptr++)
	    ;

	/* Check for quit */
	if(*ptr == 'q' || *ptr == 'Q')
	    break;

	if(isdigit(*ptr))
	{
	    value = strtod(ptr, NULL);
	    if(value >= 0. && value <= 1.)
	    {
		telem_send_value(value);
	    }
	    else
		printf("value out of range!\n");
	}
	pstate = iop_read(IO_A_REAL) & RELEASE_MASK;
	if(RELEASE_1(pstate))
	    printf("release1=1 ");
	else
	    printf("release1=0 ");

	if(RELEASE_2(pstate))
	    printf("release2=1\n");
	else
	    printf("release2=0\n");
	
    }

 done:
    return 0;
}

#endif
