/*
** arch-tag: 550f1d8c-4802-47fb-830f-a3e5ad2c9c3e
**
** MLF2 comm-mode functions.
**
*/
#include "config.h"
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
#include <setjmp.h>
#include <unistd.h>
#include <sys/fcntl.h>
#include <tt8lib.h>
#include <tt8.h>
#include <picodcf8.h>
#include "util.h"
#include "nvram.h"
#include "ioports.h"
#include "lpsleep.h"
#include "ptable.h"
#include "log.h"
#include "gps.h"
#include "atod.h"
#include "rh.h"
#include "internalpr.h"
#include "iridium.h"
#include "xmodem.h"
#include "netcdf.h"
#include "picalarm.h"
#include "message.h"
#include "sample.h"
#include "sensors.h"
#include "hash.h"
#include "fileq.h"
#include "msgq.h"
#include "compress.h"
#include "ballast.h"
#include "abort.h"
#include "comm.h"
#include "xcat.h"
#include "battery.h"

#ifdef __GNUC__
#define rename(a, b) 	_rename(a, b)
int _rename(const char* old, const char *new);
#endif

#define RUDICS_PREFIX	"00"
#define RUDICS_PN	"88160000533"
#define DIAL_PREFIX	"001"
#define HOST_PN 	"2066165701"

#define DIR_ENTRY	24
#define DIRBUF_SIZE	(DIR_ENTRY*2000L)

/* Constants for file transmission */
#define MAX_FILELEN	16
#define RX_TEMPLATE "strace -ttt -e trace=read,write -o trace rx -c -b -O %s\n"
#define MAX_RX_COMMAND MAX_FILELEN+64
#define RX_RESP_TEMPLATE "receive %s|NO CARRIER"
#define MAX_RX_RESPONSE	MAX_FILELEN+24

/* LED flasher activation */
#define FLASHER_ON()	iop_set(IO_C, 0x04)
#define FLASHER_OFF()	iop_clear(IO_C, 0x04)

#define ARGOS_ON()	iop_set(IO_C, 0x08)
#define ARGOS_OFF()	iop_clear(IO_C, 0x08)

#define MIN(a, b)	((a) < (b) ? (a) : (b))

static chat_t host_login[] = {
    { NULL, 			"login: |NO CARRIER", 	60L },
    { "mlf2\n",			"word: |NO CARRIER", 	30L },
    { "pi=3.14\n",		">> |NO CARRIER",	30L },
    { "wavelet\n",		"/no)? |NO CARRIER",	30L },
    { "yes\n",			"word: |NO CARRIER",	30L },
    { "pi=3.14\n",		"]$ |NO CARRIER",	60L },
    { "stty -echo -ixon echonl\n",	"]$ |NO CARRIER",	60L },
    { NULL, NULL, 0L }
};

static char *status_params[] = {
    "drift.iso_surface",
    NULL
};

static char *host_pn[] = {
    "2066165701",
    "2066165702",
    "2066165703",
    NULL
};

static int irsq = 0;

/* 
** Tunable parameters. All timeouts are in seconds.
*/
static short comm_checkreg = 1;
static short comm_waiting = 0;
static long comm_reg_timeout = 180;
static long comm_wait_timeout = 7200;
static long comm_timeout = 1000;
static long comm_gps_timeout = 600;
static long comm_recovery_interval = 3600;
static long comm_wait_interval = 300;
static short comm_status_throttle = 0;
static short file_queue_len = 0;
static short comm_gps_fixes = 30;
static long comm_max_disk_errors = 10;

INITFUNC(init_comm_params)
{
    add_param("comm:wait_timeout",	PTYPE_LONG, &comm_wait_timeout);
    add_param("comm:recovery_interval", PTYPE_LONG, &comm_recovery_interval);
    add_param("comm:gps_timeout",	PTYPE_LONG, &comm_gps_timeout);
    add_param("comm:status_throttle",	PTYPE_SHORT, &comm_status_throttle);
    add_param("comm:checkreg",		PTYPE_SHORT, &comm_checkreg);
    add_param("comm:reg_timeout",	PTYPE_LONG, &comm_reg_timeout);
    add_param("comm:timeout",		PTYPE_LONG, &comm_timeout);
    add_param("comm:wait_interval",	PTYPE_LONG, &comm_wait_interval);
    add_param("comm:waiting",		PTYPE_SHORT, &comm_waiting);
    add_param("comm:gps_fixes",		PTYPE_SHORT, &comm_gps_fixes);
    add_param("comm:max_disk_errors",	PTYPE_LONG, &comm_max_disk_errors);
    add_param("fileq",			PTYPE_SHORT|PTYPE_READ_ONLY, 
	      &file_queue_len);
}


static int
rudics_login(const char *prefix, const char *pn)
{
    snumber_t	sn;
    char	buf[64];

    iridium_get_sn(&sn);
    
    /* Sanity check */
    if(strlen(prefix)+strlen(pn) > sizeof(buf))
	return 0;
    sprintf(buf, "%s%s", prefix, pn);

    if(iridium_chat("AT+CBST=71,0,1\r", "OK", 6L) != 1)
	return 0;

    log_event("Dialing RUDICS %s\n", buf);
    
    if(iridium_try_dial(buf, 60L))
    {
	log_event("Connected\n");
	if(iridium_chat(NULL, "irsn: |NO CARRIER", 30L) == 1)
	{
	    sprintf(buf, "%s\n", sn_to_str(sn));
	    if(iridium_chat(buf, "]$ |NO CARRIER", 30L) == 1)
		return 1;
	    else
		log_error("comm", "No shell prompt\n");
	}
	else
	    log_error("comm", "No IRSN prompt from server\n");
    }

    log_error("comm", "Dialing error (pn = %s)\n", pn);
    return 0;
}

static void
fill_err_counts(struct elem *e, void* call_data)
{
    unsigned long	*count = (unsigned long*)e->data;
    FILE		*fp = (FILE*)call_data;

    fprintf(fp, "<e name='%s'>%ld/%ld</e>\n", e->name,
	    count[1], count[0]);
}

/*
 * write_status_file - write status message to a file
 * @type: message type, "status" or "recover".
 * @gdp: latest GPS position.
 * @pnames: list of parameters to include.
 *
 * Create an XML status message file to be sent to the remote system.
 * Returns 1 if successful, 0 if file creation fails.
 */
static int
write_status_file(char *type, GPSdata *gdp, char **pnames)
{
    int		send_counts;
    float	lat, lon;
    time_t	t;
    long	size, dfree;
    FILE	*ofp, *mfp;
    struct tm	*tm;
    HashTable	*etab;
    battery_state_t	batt[2];
    
    if((ofp = fopen("status.xml", "w")) == NULL)
    {
	log_error("comm", "Cannot open status file\n");
	return 0;
    }

    send_counts = strcmp(type, "status") ? 0 : 1;

    pdcfinfo("A:", &size, &dfree);

    t = time(0);
    tm = localtime(&t);

    /*
    ** Current date/time
    */
    fprintf(ofp, "<%s>\n", type);
    fprintf(ofp, "<date>%d-%02d-%02d %02d:%02d:%02d</date>\n",
		     tm->tm_year+1900, tm->tm_mon+1, tm->tm_mday,
		     tm->tm_hour, tm->tm_min, tm->tm_sec);

    /*
    ** GPS location and satellite count.
    */
    lat = gdp->lat.deg;
    lat += (((float)gdp->lat.min + ((float)gdp->lat.frac*1e-4))*0.0166667);
    if(gdp->lat.dir == 'S')
	lat *= -1.0;

    lon = gdp->lon.deg;
    lon += (((float)gdp->lon.min + ((float)gdp->lon.frac*1e-4))*0.0166667);
    if(gdp->lon.dir == 'W')
	lon *= -1.0;

    fprintf(ofp, "<gps nsats='%d' fmt='deg'>%.6f/%.6f</gps>\n",
	    gdp->satellites, lat, lon);
    if(gdp->status == 0)
	mq_add("Questionable GPS fix");
    
    /* Iridium signal quality */
    fprintf(ofp, "<irsq>%d</irsq>\n", irsq);
    
    /* Free disk space */
    fprintf(ofp, "<df>%ld</df>\n", dfree);

    /*
    ** Relative humidity.
    */
    if(rh_init())
    {
	short	r;

	while(!rh_dev_ready())
	    ;
	r = rh_read_data();
	rh_shutdown();

	fprintf(ofp, "<rh>%.1f</rh>\n", rh_mv_to_percent(r));
    }
    else
	mq_add("RH sensor failure");

    /*
    ** Internal pressure.
    */
    if(ipr_init())
    {
	short	r;

	while(!ipr_dev_ready())
	    ;
	r = ipr_read_data();
	ipr_shutdown();

	fprintf(ofp, "<ipr>%.1f</ipr>\n", ipr_mv_to_psi(r));
    }
    else
	mq_add("IPR sensor failure");
    
    /*
    ** Battery voltages.
    */
    read_battery(BATTERY_12v, 10, &batt[0]);
    read_battery(BATTERY_15v, 10, &batt[1]);
    fprintf(ofp, "<v>%d/%d</v>\n", (int)(batt[0].v*1000),
		     (int)(batt[1].v*1000));

    /*
    ** Accumulated piston positioning error.
    */
    fprintf(ofp, "<ep>%ld</ep>\n",
		     get_param_as_int("piston_error"));

    /*
    ** Ballast control parameters.
    */
    if(pnames != NULL)
    {
	while(*pnames)
	{
	    fprintf(ofp, "<p name='%s'>%.6g</p>\n", *pnames,
			     get_param_as_double(*pnames));
	    pnames++;
	}
    }



    /*
    ** Sensor error counters.
    */
    if(send_counts && (etab = ht_create(11, HT_STANDARD)) != 0)
    {
	sens_get_err_counters(etab);
	ht_foreach_elem(etab, fill_err_counts, ofp);
	ht_destroy(etab);
    }

    /*
    ** Include "alert" messages.
    */
    if((mfp = fopen(MESSAGE_FILE, "r")) != NULL)
    {
	int	c;
	
	while((c = fgetc(mfp)) != EOF)
	    fputc(c, ofp);
	fclose(mfp);
    }

    fprintf(ofp, "</%s>\n", type);
    fclose(ofp);

    return 1;
}

int
write_short_status(char *type, GPSdata *gdp, char *buf)
{
    char	*p;
    float	lat, lon;
    time_t	t;
    struct tm	*tm;
    battery_state_t	batt[2];
    
    p = buf;

    t = time(0);
    tm = localtime(&t);

    /*
    ** Current date/time
    */
    p += sprintf(p, "<%s>\n", type);
    p += sprintf(p, "<date>%d-%02d-%02d %02d:%02d:%02d</date>\n",
		 tm->tm_year+1900, tm->tm_mon+1, tm->tm_mday,
		 tm->tm_hour, tm->tm_min, tm->tm_sec);
    /*
    ** GPS location and satellite count.
    */
    lat = gdp->lat.deg;
    lat += (((float)gdp->lat.min + ((float)gdp->lat.frac*1e-4))*0.0166667);
    if(gdp->lat.dir == 'S')
	lat *= -1.0;

    lon = gdp->lon.deg;
    lon += (((float)gdp->lon.min + ((float)gdp->lon.frac*1e-4))*0.0166667);
    if(gdp->lon.dir == 'W')
	lon *= -1.0;

    p += sprintf(p, "<gps nsats='%d' fmt='deg'>%.6f/%.6f</gps>\n",
	    gdp->satellites, lat, lon);

    if(gdp->status == 0)
	p += sprintf(p, "<alert>Questionable GPS fix</alert>\n");

    /*
    ** Battery voltages.
    */
    read_battery(BATTERY_12v, 10, &batt[0]);
    read_battery(BATTERY_15v, 10, &batt[1]);
    p += sprintf(p, "<v>%d/%d</v>\n", (int)(batt[0].v*1000),
		 (int)(batt[1].v*1000));

    p += sprintf(p, "<alert>Disk write error</alert>\n");

    p += sprintf(p, "</%s>\n", type);
    *p = '\0';

    return (int)(p - buf);
}

/*
 * login - power-on iridium and login to remote system
 */
static int
login(void)
{
    long	t0;
    int		reg;
    char	*pn;
    static int	pn_index = 0;
    
    PET_WATCHDOG();

    if(!iridium_init(2400L))
    {
	log_error("comm", "Cannot initialize modem\n");
	return 0;
    }

    /*
     * Wait for the modem to register with the network.
     */
    if(comm_checkreg)
    {
	t0 = RtcToCtm();
	do
	{
	    reg = iridium_get_reg();
	    PET_WATCHDOG();
	} while(reg != 1 && (RtcToCtm() - t0) < comm_reg_timeout);
    
	if(reg != 1)
	{
	    log_error("comm", "Cannot register with network (%d)\n", reg);
	    iridium_shutdown();
	    return 0;
	}
    }

    /*
    ** A 30 second delay between registering and attempting a call seems to
    ** improve the odds that the call will go through.
    */
    DelayMilliSecs(30000L);
    
    /* Save the iridium signal quality value */
    irsq = iridium_get_sq();
    log_event("iridium SQ = %d\n", irsq);
    
    /*
    ** First try RUDICS and if that fails, fallback to the next modem
    ** pool number.
    */
    if(!rudics_login(RUDICS_PREFIX, RUDICS_PN))
    {
	/* Select the next modem pool phone number */
	pn = host_pn[pn_index];
	if(pn == NULL)
	{
	    pn_index = 0;
	    pn = host_pn[pn_index];
	}
	pn_index++;

	/*
	** Set the service type back to the default because rudics_login
	** changes it.
	*/
	if(iridium_chat("AT+CBST=7,0,1\r", "OK", 6L) != 1)
	{
	    log_error("comm", "Cannot set service type\n");
	    iridium_shutdown();
	    return 0;
	}

	if(!iridium_login(DIAL_PREFIX, pn, host_login))
	{
	    log_error("comm", "Cannot login to host system\n");
	    iridium_shutdown();
	    return 0;
	}

	PET_WATCHDOG();
    
	if(!iridium_dir_setup())
	{
	    log_error("comm", "Cannot setup float directory\n");
	    iridium_shutdown();
	    return 0;
	}
    }
    
    PET_WATCHDOG();
    
    return 1;
}

static void
logout(void)
{
    iridium_shutdown();
}

/*
 * send_file - send a file to the remote system.
 * @fname: file name
 *
 * Returns 1 on success, 0 on failure.
 */
static int
send_file(const char *fname)
{
    char	xmcmd[MAX_RX_COMMAND];
    char	response[MAX_RX_RESPONSE];

    if(!fileexists(fname))
	return 1;

    log_event("Uploading file %s\n", fname);    
    sprintf(xmcmd, RX_TEMPLATE, fname);
    sprintf(response, RX_RESP_TEMPLATE, fname);
    if(iridium_chat(xmcmd, response, 30L) == 1)
    {
	if(!iridium_put_file(fname))
	    return 0;
       log_event("File sent, waiting for shell prompt\n");
       return (iridium_chat(NULL, "]$ |NO CARRIER", 30L) == 1);
    }
    
    return 0;
}

/*
 * send_dir_listing - send directory listing to remote system.
 */
static void
send_dir_listing(void)
{
    char	*dirbuf, *p;
    char	cmd[32];

    if((dirbuf = malloc(DIRBUF_SIZE)) == NULL)
    {
	log_error("comm", "Cannot allocate memory for directory buffer\n");
	return;
    }

    /*
    ** Use the /m flag with the PicoDOS DIR command to dump the directory
    ** listing into memory.  Each directory entry is a fixed size string
    ** containing the filename and filesize separated by spaces.
    */
    sprintf(cmd, "dir *.* /m %lx", (long)dirbuf);
    execstr(cmd);
    for(p = dirbuf;*p;p += DIR_ENTRY)
	;
    log_event("Sending dir listing (%d bytes)\n", (int)(p - dirbuf));

    if(iridium_chat("rx -c -q 00index\n", 
		    "receive 00index|NO CARRIER", 30L) == 1)
    {
	PET_WATCHDOG();
    	iridium_put_data(dirbuf, (int)(p - dirbuf));
	iridium_chat(NULL, "]$ |NO CARRIER", 3L);
    }

    free(dirbuf);
}

/*
 * send_status - send status message to the remote system
 * @type: message type, "status" or "recover".
 * @gdp: gps position data.
 *
 * Build a status message and send it to the remote system.  First try to
 * build a full size message in a file, if that fails, an abbreviated message
 * is built in RAM. Returns non-zero if succesful and zero on failure.
 */
static int
send_status(char *type, GPSdata* gdp)
{
    static char status_buf[512];
    static int	status_sent = 0;
    static int	disk_error_counter = 0;
    int		n, r;

    /*
     * The 'status_sent' flag provides a way to throttle the sending of
     * status messages while the float is waiting on the surface.
     */
    if(comm_waiting && status_sent)
    {
	status_sent--;
	return 1;
    }
    
    r = 0;
    
    if(write_status_file(type, gdp, status_params))
    {
	if(disk_error_counter > 0)
	    disk_error_counter--;
	
	if(send_file("status.xml"))
	{
	    status_sent = comm_status_throttle;
	    r = 1;
	}
    }
    else
    {
	/*
	 * If we could not write the status file, build a short status
	 * message in memory and send that.
	 */
	disk_error_counter++;
	mq_add("Disk write failed");
	if(disk_error_counter >= comm_max_disk_errors)
	    mq_add("Aborting mission");
	
	n = write_short_status(type, gdp, status_buf);
	if(iridium_chat("rx -c -q status.xml\n", 
			"receive status.xml|NO CARRIER", 30L) == 1)
	{
	    PET_WATCHDOG();
	    iridium_put_data(status_buf, n);
	    if(iridium_chat(NULL, "]$ |NO CARRIER", 3L) == 1)
	    {
		status_sent = comm_status_throttle;
		r = 1;
	    }
	}

    }

    /*
    ** If we are having disk write errors, the float will not be able
    ** to upload commands from shore. All we can do is abort the mission
    ** and hope the reset fixes the problem...
    */
    if(disk_error_counter >= comm_max_disk_errors)
	abort_mission(0);
    
    return r;
}

/*
 * send_telem - send the telemetry file to the remote system.
 *
 * The term "telemetry file" is historical and refers to the subsampled
 * environmental data file. Returns 1 if succesful, 0 on failure.
 */
static int
send_telem(void)
{
    if(!fileexists("telem.ncz"))
	return 1;
    
    if(send_file("telem.ncz"))
    {
	unlink("telem.ncz");
	return 1;
    }

    log_error("comm", "File send failed (telem.ncz)\n");

    return 0;
}

/*
 * run_dataproc - run data processing program
 *
 * Send commands to the remote system to run the data processing
 * program.  This function should be called after every file upload.
 */
static int
run_dataproc(void)
{
    snumber_t	sn;
    char	cmd[40];

    iridium_get_sn(&sn);
    sprintf(cmd, "dataproc %s\n", sn_to_str(sn));
    
    PET_WATCHDOG();
    
    if(iridium_chat(cmd, "]$ |NO CARRIER", 30L) != 1)
    {
	log_error("comm", "Lost connection\n");
	return 0;
    }

    PET_WATCHDOG();
    return 1;
}


/*
 * send_data - send the next data file to the remote system.
 *
 * Remove the next file from the queue (oldest file) and try to send it to 
 * the remote system. Returns 1 if successful or 0 on failure.
 */
static int
send_data(void)
{
    return fq_send(send_file, 0);
}

/*
 * get_cmd_file - download the command file
 *
 * Download the command file from the remote site.  Returns 1 if the
 * transfer was successful or if the file was not present. Returns 0
 * if the transfer fails.
 */
static int
get_cmd_file(void)
{
    int		r;
    snumber_t	sn;
    char	cmd[40];

    iridium_get_sn(&sn);
    sprintf(cmd, "getmessages %s\n", sn_to_str(sn));
    
    if(iridium_chat(cmd, "]$ |NO CARRIER", 30L) != 1)
    {
	log_error("comm", "Lost connection\n");
	return 0;
    }

    PET_WATCHDOG();
    unlink("msg.xml");
    
    r = iridium_chat("sx -q msg.xml\n", "command now.|NO CARRIER|]$ ",
		     30L);
    PET_WATCHDOG();
    
    switch(r)
    {
	case 0:
	    log_error("comm", "Timeout\n");
	    break;
	case 1:
	    if(iridium_get_file("msg.xml"))
		return 1;
	    else
	    {
		/* transfer failed, remove empty file */
		unlink("msg.xml");
		return 0;
	    }
	    break;
	case 2:
	    log_error("comm", "Lost connection\n");
	    break;
	case 3:
	    /* File not found. Not an error */
	    return 1;
	    break;
    }
    return 0;
}

/*
 * reply_text - insert text into "reply" file
 * @text: a printf-style format string.
 */
static void
reply_text(const char *text, ...)
{
    va_list	args;
    FILE	*fp;
    
    va_start(args, text);
    if((fp = fopen(XML_TEXT_FILE, "w")) != NULL)
    {
	vfprintf(fp, text, args);
	fclose(fp);
    }
}

/*
 * handle_command - callback to handle commands
 * @tag: command name
 * @data: command arguments
 *
 * Callback to handle processing commands in the XML command file
 * retrieved from the remote system.
 */
static void
handle_command(const char *tag, const char *data)
{
    int		n, limit;
    long	larg, larg2;
    const char	*src;
    char	 *dst;
    char	fname[16];
    
    if(!strcmp(tag, "abort"))
    {
	log_event("Got abort command\n");
	abort_mission(1);
    }
    else if(!strcmp(tag, "safe"))
    {
	larg = larg2 = 0;
	if(sscanf(data, "%ld %ld", &larg, &larg2) == 2)
	{
	    enter_safe_mode(larg, larg2);
	}
	else
	{
	    log_error("comm", "Missing arguments to !safe command\n");
	    reply_text("Missing arguments to !safe command");
	}
    }
    else if(!strcmp(tag, "restart"))
    {
	log_event("Restarting the mission");
	restart_sys();
    }
    else if(!strcmp(tag, "recover"))
    {
	log_event("Entering recovery mode");
	enter_recovery_mode();
    }
    else if(!strncmp(tag, "send-file", 9L))
    {
	PET_WATCHDOG();
	
	/* Remove leading and trailing spaces from filename */
	src = data;
	dst = fname;
	n = 0;
	limit = sizeof(fname) - 1;
	while(!isspace(*src) && n < limit)
	{
	    *dst++ = *src++;
	    n++;
	}
	*dst = '\0';
        if(!strcmp(tag, "send-file-next"))
	{
	    n = fq_add_next(fname);
	    reply_text("%s queued at front (qlen = %d)", fname, n);
	    file_queue_len = n;
	}
	else
	{
	    n = fq_add(fname);	
	    reply_text("%s queued (qlen = %d)", fname, n);
	    file_queue_len = n;
	}
    }
    else if(!strcmp(tag, "del"))
    {
	src = data;
	dst = fname;
	n = 0;
	limit = sizeof(fname) - 1;
	while(!isspace(*src) && n < limit)
	{
	    *dst++ = *src++;
	    n++;
	}
	*dst = '\0';
	if(!strncmp(&fname[n-3], ".nc", 3L))
	{
	    unlink(fname);
	    reply_text("%s removed", fname);
	}
	
    }
    else if(!strcmp(tag, "dir"))
	send_dir_listing();
    else if(!strcmp(tag, "wait"))
    {
	log_event("Waiting for further instructions\n");
	reply_text("In wait mode");
	if(!comm_waiting)
	    mq_add("Waiting on surface");
	comm_waiting = 1;
    }
    else if(!strcmp(tag, "continue"))
    {
	log_event("Wait completed\n");
	reply_text("Mission continuing");
	if(comm_waiting && fileexists(MESSAGE_FILE))
	    unlink(MESSAGE_FILE);
	comm_waiting = 0;
    }
    else if(!strcmp(tag, "get-errorz"))
    {
	/* 
	 * Copy error messages to a compressed file and queue
	 * for sending.
	 */
	larg = 0;
	sscanf(data, "%ld", &larg);
	log_event("Extracting log error messages\n");
	log_copy_errors("errors.txt", larg);
	file_queue_len = fq_add_next("errors.txt");
	reply_text("errors file queued for upload.");
    }
    else if(!strcmp(tag, "get-errors"))
    {
	/* Return error messages in the reply */
	larg = 0;
	sscanf(data, "%ld", &larg);
	log_event("Extracting log error messages\n");
	log_copy_errors(XML_TEXT_FILE, larg);
    }
    else if(!strcmp(tag, "flasher-on"))
    {
	FLASHER_ON();
    }
    else if(!strcmp(tag, "flasher-off"))
    {
	FLASHER_OFF();
    }
    
}

/*
 * ignore_command - dummy callback.
 */
static void
ignore_command(const char *tag, const char *data)
{
}

/*
 * exec_commands - fetch and interpret commands from the remote system
 * @allow_abort: if non-zero, allow 'abort' commands.
 *
 * Returns 1 of success, 0 on failure.
 */
static int
exec_commands(int allow_abort)
{
    FILE	*ifp, *ofp;
    int		r;
    
    if(get_cmd_file())
    {
	if(!fileexists("msg.xml"))
	    return 1;
	
	/*
	** Parse the command file and create the reply file.
	*/
	ifp = ofp = NULL;
	r = 0;
	if((ifp = fopen("msg.xml", "r")) != NULL &&
	   (ofp = fopen("reply.xml", "w")) != NULL)
	{
	    msg_init_parser();
	    if(allow_abort)
		msg_add_handler("abort", handle_command);
	    else
		msg_add_handler("abort", ignore_command);
	    msg_add_handler("send-file", handle_command);
	    msg_add_handler("send-file-next", handle_command);
	    msg_add_handler("dir", handle_command);
	    msg_add_handler("wait", handle_command);
	    msg_add_handler("continue", handle_command);
	    msg_add_handler("del", handle_command);
	    msg_add_handler("get-errors", handle_command);
	    msg_add_handler("get-errorz", handle_command);
	    msg_add_handler("flasher-on", handle_command);
	    msg_add_handler("flasher-off", handle_command);
	    msg_add_handler("restart", handle_command);
	    msg_add_handler("safe", handle_command);
	    msg_add_handler("recover", handle_command);
	    
	    r = msg_parse_file(ifp, ofp, 0);
	}

	if(ofp)
	    fclose(ofp);
	if(ifp)
	    fclose(ifp);
	unlink("msg.xml");

	PET_WATCHDOG();
	return 1;
    }
    
    return 0;
}

/** 
 * comm_mode - communication procedure
 * @type: status message type ("status" or "recover")
 * @gps_on: if != 0, GPS is already powered-on.
 *
 * This function runs the communication procedure with the remote system on
 * shore through the Iridium modem. The procedure is as follows:
 *
 *    1. Read latest GPS position.
 *    2. Power-on iridium modem and login to remote system.
 *    3. Create and upload status message.
 *    4. Download and execute command file.
 *    5. Upload reply file from step #4
 *    6. Upload subsampled environmental data file.
 *    7. Upload next data file from file queue.
 *
 * If a communication failure occurs during steps #3-#7, the modem is
 * powered-off and control is passed to step #2 and then back to the step
 * where the failure occured. The procedure exits when all steps have been
 * performed or when the process times out. Timeout is set with the
 * "comm:timeout" parameter.
 *
 * If the float is "waiting" (comm:waiting is set), the above seven steps are
 * repeated until comm:wait_timeout expires. Another "wait mode" change is
 * that all intervening steps are re-executed if a communication failure
 * occurs (i.e. there is no jump back to the step where the failure
 * occurs). This is to prevent the float getting "stuck" trying to send a
 * large data file and never getting around to sending status updates or
 * checking for commands.
 */
void
comm_mode(char *type, int gps_on)
{
    long		t0, timeout, login_time, dt;
    int			allow_abort, first_fix = 1;
    long		login_wait, login_max_wait = 24000;
    double		backoff_factor = 1.4142;
    GPSdata		gd;
    char		pbuf[32];
    enum {ST_LOGIN=0,
	  ST_STATUS,
	  ST_POST_STATUS,
	  ST_CMD,
	  ST_POST_CMD,
	  ST_TELEM,
	  ST_POST_TELEM,
	  ST_DATA,
	  ST_POST_DATA,
	  ST_GPS,
	  ST_DONE} comm_state, next_state;

    ARGOS_ON();
    FLASHER_ON();
    
    if(!xcat_setup(pbuf, 31))
	xcat_setup(pbuf, 31);
    
    comm_state = ST_GPS;
    next_state = ST_STATUS;
    login_time = 0;
    
    t0 = RtcToCtm();
    if(comm_waiting)
    {
	timeout = comm_wait_timeout;
	mq_add("Waiting on surface");
    }
    else
	timeout = comm_timeout;
		    
    if(!strcmp(type, "recover"))
	allow_abort = 0;
    else
	allow_abort = 1;

    if(fileexists("telem.nc"))
    {
	if(compress_file("telem.nc", "telem.ncz"))
	    unlink("telem.nc");
    }
    
    if(fileexists("dataql.sx"))
    {
	if(compress_file("dataql.sx", "dataql.sxz"))
	    unlink("dataql.sx");
    }

    file_queue_len = fq_len();

    /* Wait 8000ms before retrying to login */
    login_wait = 8000L;
    
    while(comm_state != ST_DONE && (RtcToCtm() - t0) < timeout)
    {
	PET_WATCHDOG();
	
	switch(comm_state)
	{
	    case ST_GPS:
		logout();
		memset(&gd, 0, sizeof(gd));
		if(!gps_on)
		    gps_on = gps_init();

		if(!gps_on)
		{
		    log_error("comm", "GPS initialization failed\n");
		    mq_add("GPS failed");
		}
		else
		{
		    sens_open_data_file(GPS_FILE, 0);
		    sens_wait_for_gps(&gd, comm_gps_timeout, 1, comm_gps_fixes);
		    sens_write_gps_fix(&gd);
		    sens_close_file(GPS_FILE);
		    gps_shutdown();
		    gps_on = 0;
		}

		/* 
		** Reset the timer after the first GPS fix so the GPS wait time
		** doesn't affect the COMM-mode timeout.
		*/
		if(first_fix) 
		{
		    t0 = RtcToCtm();
		    first_fix = 0;
		}
		
		if(comm_waiting)
		    timeout = comm_wait_timeout;
		else
		    timeout = comm_timeout;
		comm_state = ST_LOGIN;
		break;
	    case ST_LOGIN:
		/* power-on iridium and login */
		logout();
		if(comm_waiting)
		{
		    /*
		     * Throttle the login rate during wait mode to reduce
		     * power consumption.
		     */
		    dt = login_time + comm_wait_interval - RtcToCtm();
		    if(dt > 0)
		    {
			log_event("Sleeping until next login time (%ld seconds)\n", dt);
			while(dt > 0)
			{
			    /* Sleep in 30s chunks so we can service the watchdog */
			    PET_WATCHDOG();
			    if(isleep(MIN(dt, 30)) == 1)
			    {
				log_event("Got abort signal\n");
				abort_prog();
			    }
			    dt -= 30;
			}
		    }
		}
		
		if(login() == 0)
		{
		    /*
		     * If the login process fails and we are waiting, set the
		     * next state to ST_GPS so we can update the GPS position,
		     * otherwise apply an exponential backoff to the login-delay
		     * and try again.
		     */
		    if(comm_waiting)
			comm_state = ST_GPS;
		    else
		    {
			comm_state = ST_LOGIN;
			DelayMilliSecs(login_wait);
			/* Exponential back-off */
			login_wait = (double)login_wait*backoff_factor;
			if(login_wait > login_max_wait)
			    login_wait = login_max_wait;
		    }
		}
		else
		{
		    /* 
		     * If we are waiting, force a pass through all of the
		     * intermediate states rather than a jump to the
		     * saved 'next_state'. This insures that we don't get
		     * "stuck" trying to send a large data file and thus
		     * never get a chance to send an updated status message
		     * or to check for a command file.
		     */
		    if(comm_waiting)
			next_state = ST_STATUS;
		    login_time = RtcToCtm();
		    /* Reset login wait time to 8 seconds */
		    login_wait = 8000;
		    comm_state = next_state;
		}
		break;
	    case ST_STATUS:
		/* send status message */
		next_state = comm_state;

		if(send_status(type, &gd) == 0)
		    comm_state = ST_LOGIN;
		else
		    comm_state = ST_POST_STATUS;
		break;
	    case ST_POST_STATUS:
		next_state = comm_state;
		if(run_dataproc() == 0)
		    comm_state = ST_LOGIN;
		else
		    comm_state = ST_CMD;
		break;
	    case ST_CMD:
		/* fetch commands */
		next_state = comm_state;
		if(exec_commands(allow_abort) == 0)
		    comm_state = ST_GPS;
		else
		    comm_state = ST_POST_CMD;
		/* 
		 * comm_waiting may have been set by the command file so
		 * we need to check it and adjust the timeout if
		 * necessary.
		 */
		if(comm_waiting)
		    timeout = comm_wait_timeout;
		else
		    timeout = comm_timeout;
		break;
	    case ST_POST_CMD:
		next_state = comm_state;
		if(send_file("reply.xml") == 0 || run_dataproc() == 0)
		    comm_state = ST_GPS;
		else
		{
		    if(fileexists("reply.xml"))
			unlink("reply.xml");
		    comm_state = ST_TELEM;
		}
		break;
	    case ST_TELEM:
		/* send telemetry file */
		next_state = comm_state;
		if(send_telem() == 0)
		    comm_state = ST_GPS;
		else
		    comm_state = ST_POST_TELEM;
		break;
	    case ST_POST_TELEM:
		next_state = comm_state;
		if(fileexists("dataql.sxz"))
		{
		    if(send_file("dataql.sxz"))
			unlink("dataql.sxz");
		}

		if(run_dataproc() == 0)
		    comm_state = ST_GPS;
		else
		    comm_state = ST_DATA;
		break;
	    case ST_DATA:
		/* send data file */
		next_state = comm_state;
		if(send_data() == 0)
		    comm_state = ST_GPS;
		else
		{
		    run_dataproc();
		    comm_state = ST_POST_DATA;
		}
		break;
	    case ST_POST_DATA:
		next_state = comm_state;
		if((run_dataproc() == 0) || comm_waiting)
		    comm_state = ST_GPS;
		else 
		{
		    if(fq_len() > 0)
			comm_state = ST_DATA;
		    else
			comm_state = ST_DONE;
		}
		break;
	    case ST_DONE:
		break;
	}

    }

    logout();


    if(fileexists("telem.ncz"))
    {
	int	i;
	char newname[12];
	
	/* 
	 * We could not send the quick-look file, rename it and
	 * queue it for later delivery.
	 */
	for(i = 0;i < 100;i++)
	{
	    sprintf(newname, "telem%02d.ncz", i);
	    if(!fileexists(newname))
	    {
		rename("telem.ncz", newname);
		fq_add(newname);
		break;
	    }
	}
	
	if(fileexists("telem.ncz"))
	{
	    log_error("comm", "Too many quick-look files saved\n");
	    unlink("telem.ncz");
	}
	
    }
    
    if(fileexists(MESSAGE_FILE))
	unlink(MESSAGE_FILE);
    
    /* Read and store the final GPS position */
    memset(&gd, 0, sizeof(gd));
    if(!gps_on)
	gps_on = gps_init();

    if(!gps_on)
	log_error("comm", "GPS initialization failed\n");
    else
    {
	sens_open_data_file(GPS_FILE, 0);
	sens_wait_for_gps(&gd, comm_gps_timeout, 1, comm_gps_fixes);
	sens_write_gps_fix(&gd);
	sens_close_file(GPS_FILE);
	gps_shutdown();
    }

    /* 
     * Make sure flasher is not left active unless we are
     * in recovery mode.
     */
    if(strcmp(type, "recover"))
    {
	ARGOS_OFF();
	FLASHER_OFF();
    }
    
}

/**
 * comm_wait - comm-mode with a forced wait on the surface.
 * @msg: extra message to send
 * @gps_on: if non-zero, GPS is slready powered-on.
 *
 */
void
comm_wait(int gps_on)
{
    comm_waiting = 1;
    comm_mode("status", gps_on);
}

void
comm_recovery_mode(void)
{
    long	last_time = 0L;

    comm_waiting = 0;
    
    while(1)
    {
	PET_WATCHDOG();
	/*
	 * Wait in low-power mode for the next communication interval. While
	 * waiting, wake every 60 seconds and print a prompt to the console to
	 * allow the user to interrupt the wait and perform an orderly restart
	 * of the system.
	 */
	while((RtcToCtm() - last_time) < comm_recovery_interval)
	{
	    PET_WATCHDOG();
	    fputs("Waiting to send RECOVERY message\n", stdout);
	    if(isleep(60L) == 1)
	    {
		fputs("Control program will now restart\n", stdout);
		abort_prog();
	    }
	}

	last_time = RtcToCtm();
	comm_mode("recover", 0);

    }
    
}
