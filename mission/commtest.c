/*
** arch-tag: 531ca2c5-a6ea-4d53-9e11-7f43933678b8
** Time-stamp: <2005-10-10 22:35:52 mike>
**
** Test COMM-mode
**
*/
#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <string.h>
#include <tt8.h>
#include <tt8lib.h>
#include <tpu332.h>
#include <tat332.h>
#include <sim332.h>
#include <qsm332.h>
#include <dio332.h>
#include <tt8pic.h>
#include <picodcf8.h>
#include "ptable.h"
#include "comm.h"
#include "fileq.h"
#include "log.h"
#include "msgq.h"

INITFUNC(sysinit)
{
    /*
    ** Hardware initialization
    */
    InitTT8(NO_WATCHDOG, TT8_TPU);
    InitCF8(CF8StdCS, CF8StdAddr);
    if(errno != 0)
	printf("\nWARNING: InitCF8 failed, error %d\n", errno);
    if(errno == -1 || errno == PiDosNoHardware)
	exit(1);
    
    SimSetFSys(16000000L);
}

int
main(int ac, char *av[])
{
    openlog("commlog.txt");
    
    set_param_int("comm:gps_timeout", 30L);
    set_param_int("comm:timeout", 1200L);

    log_error("test", "This is a test error message\n");
    log_event("This is a test event message\n");
    log_error("test", "This is another test error message\n");
    unlink("gps0001.nc");
    fq_add("env0001.nc");
    mq_add("This is a test alert");
    
    comm_mode("status", 0);
    unlink("env0001.ncz");
    
    return 0;
}
