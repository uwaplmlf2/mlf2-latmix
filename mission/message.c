/*
** $Id: message.c,v 0f54b8ad013b 2008/04/17 17:17:28 mikek $
**
** Functions for sending and receiving XML messages.
**
*/
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/fcntl.h>
#include <string.h>
#include <tt8lib.h>
#include <tt8.h>
#include <tpu332.h>
#include <qsm332.h>
#include <sim332.h>
#include <picodcf8.h>
#include "hash.h"
#include "ptable.h"
#include "log.h"
#include "xmlparse.h"
#include "message.h"


struct cbuf {
    long	size;
    long	i;
    char	*buf;
};


static XML_Parser	parser;

/*
** XML parser callback functions.  The functions are used to parse
** incoming messages with the following DTD.
**
** <!ELEMENT msg (set*, get*)>
** <!ELEMENT set EMPTY>
** <!ELEMENT get EMPTY>
** <!ATTLIST set name CDATA #REQUIRED
**               value CDATA #REQUIRED>
** <!ATTLIST get name CDATA #REQUIRED>
** <!ATTLIST msg id CDATA #IMPLIED>
**
** Each "set" element processed results in an update of the resulting
** parameter.  All parameter names (from the "name" attribute) are stored
** in a Hash Table for use in building the reply message.  The reply
** contains the current settings of all of the <msg> parameters in the
** following format.
**
** <!ELEMENT reply (p*)>
** <!ELEMENT p (PCDATA)>
** <!ATTLIST p name CDATA #REQUIRED>
** <!ATTLIST reply id CDATA #IMPLIED>
*/

/* Hash table for parameter value requests */
static HashTable *preq;

/* Hash table for processing-instruction callbacks */
static HashTable *pi_cmds;

static int msg_skip_dups = 1;
static char _msgid[128];
static char _msgbuf[1024];

static void
start_element(void *udata, const char *name, const char **atts)
{
    int		*state = (int*)udata;
    const char	*pname, *pval;
    
    if(!strcmp(name, "msg"))
    {
	(*state)++;
	if(atts && !strcmp(atts[0], "id"))
	{
	    if(msg_skip_dups && !strcmp(_msgid, atts[1]))
	    {
		/* We already saw this message, skip it */
		*state = 0;
		log_event("Skipping duplicate message\n");
	    }
	    
	    strncpy(_msgid, atts[1], sizeof(_msgid)-1);
	}
	else
	    _msgid[0] = '\0';
	
    }
    

    /*
    ** Make sure we are inside a non-nested <msg> tag before we allow
    ** any processing of other elements.
    */
    if(*state != 1)
	return;
    
    if(!strcmp(name, "set"))
    {
	pname = pval = 0;
	
	while(**atts)
	{
	    if(!strcmp(atts[0], "name"))
		pname = atts[1];
	    else if(!strcmp(atts[0], "value"))
		pval = atts[1];
	    atts += 2;
	}
	
	if(pname && pval)
	{
	    set_param_str(pname, pval);
	    ht_insert_elem(preq, pname, NULL, 0);
	}
	else
	    log_error("msg-parse", "Message format error\n");
	
    }
    else if(!strcmp(name, "get") && !strcmp(atts[0], "name"))
	ht_insert_elem(preq, atts[1], NULL, 0);
    
}

static void
end_element(void *udata, const char *name)
{
    int	*state = (int*)udata;

    if(!strcmp(name, "msg"))
	(*state)--;
}

static void
char_data(void *udata, const char *s, int len)
{
}

static void
process_instr(void *udata, const char *tag, const char *data)
{
    int	*state = (int*)udata;
    pi_handler	handler;
    
    if(*state == 0)
	return;
    
    if(ht_find_elem(pi_cmds, tag, (void**)&handler, NULL) && handler)
	(*handler)(tag, data);
}


/*
 * sprint_param - HashTable callback to build reply message in a string.
 */
static void
sprint_param(struct elem *e, void *calldata)
{
    struct cbuf		*bp = (struct cbuf*)calldata;
    struct param	param;
    int			n;
    unsigned		type;
    
    if(!get_param(e->name, &param))
	return;
    
    n = strlen(e->name) + 11;
    if((bp->i + n) >= bp->size)
	return;
    bp->i += sprintf(&bp->buf[bp->i], "<p name='%s'>", e->name);
    type = param.type & ~PTYPE_SIZEMASK;

    switch(type & PTYPE_TYPEMASK)
    {
	case PTYPE_DOUBLE:
	    n = 26;
	    if((bp->i + n) >= bp->size)
		return;
	    bp->i += sprintf(&bp->buf[bp->i], "%.8g</p>\n", 
			     *((double*)param.loc));
	    break;
	case PTYPE_LONG:
	    n = 15;
	    if((bp->i + n) >= bp->size)
		return;
	    bp->i += sprintf(&bp->buf[bp->i], "%ld</p>\n", 
			     *((long*)param.loc));
	    break;
	case PTYPE_SHORT:
	    n = 10;
	    if((bp->i + n) >= bp->size)
		return;
	    bp->i += sprintf(&bp->buf[bp->i], "%hd</p>\n", 
			     *((short*)param.loc));
	    break;
    }
}

/*
 * fprint_param - HashTable callback to build reply message in a file.
 */
static void
fprint_param(struct elem *e, void *calldata)
{
    FILE		*fp = (FILE*)calldata;
    struct param	param;
    unsigned		type;
    
    if(!get_param(e->name, &param))
	return;

    fprintf(fp, "<p name='%s'>", e->name);

    type = param.type & ~PTYPE_SIZEMASK;
    switch(type & PTYPE_TYPEMASK)
    {
	case PTYPE_DOUBLE:
	    fprintf(fp, "%.8g</p>\n", *((double*)param.loc));
	    break;
	case PTYPE_LONG:
	    fprintf(fp, "%ld</p>\n", *((long*)param.loc));
	    break;
	case PTYPE_SHORT:
	    fprintf(fp, "%hd</p>\n", *((short*)param.loc));
	    break;
    }
}

static void
_cleanup_tables(void)
{
    if(preq)
	ht_destroy(preq);
    if(pi_cmds)
	ht_destroy(pi_cmds);

    pi_cmds = preq = NULL;
}

static int
_initialize_tables(void)
{
    _cleanup_tables();

    if((pi_cmds = ht_create(11, HT_NOCASE)) == NULL)
    {
	log_error("parse_message", "Out of memory!\n");
	return 0;
    }

    if((preq = ht_create(11, HT_NOCASE)) == NULL)
    {
	log_error("parse_message", "Out of memory!\n");
	_cleanup_tables();
	return 0;
    }


    return 1;
}



/**
 * msg_init_parser - initialize message parser.
 *
 * Initialize all data structures associated with parsing the incoming
 * messages.  Returns 1 if successful, otherwise 0.
 */
int
msg_init_parser(void)
{
    if(parser)
	XML_ParserFree(parser);
    
    parser = XML_ParserCreate(NULL);

    if(!_initialize_tables())
	return 0;
    
    XML_SetElementHandler(parser, start_element, end_element);
    XML_SetCharacterDataHandler(parser, char_data);
    XML_SetProcessingInstructionHandler(parser, process_instr);

    return 1;
}

/**
 * msg_add_handler - add a handler for a processing instruction.
 * @tag: process-instruction tag
 * @handler: callback function
 *
 * This function allows the upper level communications functions to
 * register a handler for specific processing instructions found in
 * an incoming message.  When called, @handler will be passed two
 * arguments.  The first is the tag name (@tag) the second is the
 * data portion of the instruction.
 */
void
msg_add_handler(const char *tag, pi_handler handler)
{
    if(!pi_cmds)
	return;
    ht_insert_elem(pi_cmds, tag, handler, 0);
}



/**
 * msg_parse_message - parse an incoming message and format a reply.
 * @msg: string containing message.
 * @msglen: message length.
 * @reply: buffer for reply.
 * @rlen: maximum length of reply.
 * @allow_dup: duplicate handling flag
 *
 * Parse an incoming message and format a reply message.  @msg and
 * @reply may be stored in the same location because the input is
 * evaluated in its entirety before the reply is written.  If
 * @allow_dup is non-zero, the message will be processed even if
 * it is a duplicate of the previous message.
 *
 * Returns the actual length of @reply or 0 if an error occurs.
 */
int
msg_parse_message(char *msg, int msglen, char *reply, int rlen, int allow_dup)
{
    int		state;
    FILE	*pfp;
    struct cbuf	strbuf;
    
    strbuf.size = rlen;
    strbuf.buf = reply;
    strbuf.i = 0;

    msg_skip_dups = (allow_dup == 0);
    
    state = 0;
    XML_SetUserData(parser, &state);
    if(!XML_Parse(parser, msg, msglen, 1)) 
    {
	log_error("parse_message",
		"%s at line %d\n",
		XML_ErrorString(XML_GetErrorCode(parser)),
		XML_GetCurrentLineNumber(parser));
	return 0;
    }


    /*
    ** Dump the parameter table again so we have a record of the
    ** updates.
    */
    if((pfp = fopen("params.xml", "r+")) != NULL)
    {
	fseek(pfp, 0L, SEEK_END);
	dump_params(pfp);
	fclose(pfp);
    }
    
    /*
    ** Construct the reply message.
    */
    strbuf.i += sprintf(reply, "<reply id='%s'>\n", _msgid);
    ht_foreach_elem(preq, sprint_param, (void*)&strbuf);
    
    if((strbuf.i + 8) < rlen)
	strbuf.i += sprintf(&reply[strbuf.i], "</reply>");
    reply[strbuf.i] = '\0';
    
    return strbuf.i;
}

/**
 * msg_parse_file - parse an incoming message stored in a file
 * @ifp: pointer to message file.
 * @ofp: pointer to reply file.
 * @allow_dup: duplicate handling flag
 *
 * Parse an incoming message from a file and format a reply.  If @allow_dup is
 * non-zero, the message will be processed even if it is a duplicate of the
 * previous message.  Returns 1 if successful or 0 if an error occurs.
 */
int
msg_parse_file(FILE *ifp, FILE *ofp, int allow_dup)
{
    int		state, done, c;
    char	*p;
    FILE	*pfp, *xfp;
    
    state = 0;
    XML_SetUserData(parser, &state);
    done = 0;
    msg_skip_dups = (allow_dup == 0);
    do
    {
	size_t len = fread(_msgbuf, 1L, sizeof(_msgbuf), ifp);

	/* Check for CTRL-z end-of-file marker */
	if((p = strchr(_msgbuf, '\032')) != NULL)
	    len = p - _msgbuf;
	
	done = len < sizeof(_msgbuf);
	if(!XML_Parse(parser, _msgbuf, len, done)) 
	{
	    log_error("parse_message",
		      "%s at line %d\n",
		      XML_ErrorString(XML_GetErrorCode(parser)),
		      XML_GetCurrentLineNumber(parser));
	    return 0;
	}
    } while(!done);


    /*
    ** Dump the parameter table again so we have a record of the
    ** updates.
    */
    if((pfp = fopen("params.xml", "a")) != NULL)
    {
	dump_params(pfp);
	fclose(pfp);
    }
    
    /*
    ** Construct the reply message.
    */
    fprintf(ofp, "<reply id='%s'>\n", _msgid);
    ht_foreach_elem(preq, fprint_param, (void*)ofp);

    /* See if there is any text to add */
    if((xfp = fopen(XML_TEXT_FILE, "r")) != NULL)
    {
	fputs("<text>\n", ofp);
	while((c = fgetc(xfp)) != EOF)
	    fputc(c, ofp);
	fputs("\n</text>\n", ofp);
	fclose(xfp);
	unlink(XML_TEXT_FILE);
    }
    fputs("</reply>", ofp);
    
    return 1;
}

#ifdef TEST

static double drift_compress = 42.;
static double drift_air = 33.2;
static double ballast_v0 = 1.5;
static long settle_seek_time = 3600;
static long comm_gps_timeout = 10;

static void
send_data_handler(const char *tag, const char *data)
{
    printf("Got %s command: %s\n", tag, data);
}


int main(void)
{
    FILE *ifp, *ofp, *pfp;
    int		i;
    
    InitTT8(NO_WATCHDOG, TT8_TPU);
    InitCF8(CF8StdCS, CF8StdAddr);
    if(errno != 0)
	printf("\nWARNING: InitCF8 failed, error %d\n", errno);
    if(errno == -1 || errno == PiDosNoHardware)
	return 0;

    init_param_table();
    
    add_param("drift.compress", PTYPE_DOUBLE, &drift_compress);
    add_param("drift.air", PTYPE_DOUBLE, &drift_air);
    add_param("ballast.v0", PTYPE_DOUBLE, &ballast_v0);
    add_param("settle.seek_time", PTYPE_LONG, &settle_seek_time);
    add_param("comm:gps_timeout", PTYPE_LONG, &comm_gps_timeout);

    if((pfp = fopen("params.xml", "w")) != NULL)
    {
	fseek(pfp, 0L, SEEK_END);
	dump_params(pfp);
	fclose(pfp);
    }


    for(i = 0;i < 100;i++)
    {
	msg_init_parser();
	msg_add_handler("send-file", send_data_handler);
	
	if((ifp = fopen("msg.xml", "r")) == NULL)
	{
	    printf("Cannot open msg.xml\n");
	    return 0;
	}

	if((ofp = fopen("reply.xml", "w")) == NULL)
	{
	    printf("Cannot open reply.xml\n");
	    fclose(ifp);
	    return 0;
	}


	msg_parse_file(ifp, ofp, 1);
	fclose(ofp);
	fclose(ifp);
	DelayMilliSecs(2000L);
    }
        
    DelayMilliSecs(200L);
    
    return 0;
}

#endif
