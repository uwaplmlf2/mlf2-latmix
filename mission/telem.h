/*
** arch-tag: 72a86b51-a68d-4db1-82ca-b1a9be7b68c0
*/
#ifndef _TELEM_H_
#define _TELEM_H_

#define TELEM_CMD_NULL		0
#define TELEM_CMD_SURFACE	1
#define TELEM_CMD_DOWN		2
#define TELEM_CMD_UP		3

void telem_send_value(double value);
int telem_get_command(void);
void telem_set_sense(int which);
void telem_clear_sense(int which);

#endif /* _TELEM_H_ */
