/*
**
** MLF2 safe-mode control program.
**
**
*/
#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <errno.h>
#include <tt8lib.h>
#include <tpu332.h>
#include <tt8.h>
#include <tat332.h>
#include <sim332.h>
#include <qsm332.h>
#include <dio332.h>
#include <tt8pic.h>
#include <picodcf8.h>
#include "util.h"
#include "ioports.h"
#include "lpsleep.h"
#include "log.h"
#include "nvram.h"
#include "newpr.h"
#include "gps.h"
#include "motor.h"
#include "ptable.h"
#include "ballast.h"
#include "sample.h"
#include "msgq.h"
#include "mctl.h"
#include "comm.h"
#include "drogue.h"
#include "version.h"
#include "libversion.h"

#define MK_ID()	"$Id: " ## REVISION ## " " ## __DATE__ ## " " ## __TIME__ ## " $"

static const char *__id__ = MK_ID();

#define NV_ISSET(name, nvp)	(nv_lookup(name, (nvp)) != -1 && (nvp)->l != 0)

#ifdef __GNUC__
static void bailout(void) __attribute__ ((noreturn));
#endif

static long	cpu_clock = 4;
extern char	*_mtop, *_mbot, *_mcur;
extern unsigned long 	_H0_org;

static void
mlf2_qspi_init(void)
{
    register unsigned 	cs_mask, cntl_mask;
    
    cs_mask   = M_PCS0 | M_PCS1 | M_PCS2 | M_PCS3;
    cntl_mask = M_SCK | M_MOSI | M_MISO;
    
    /*
    ** Configure the QSPI control and chip select lines as
    ** general purpose outputs.  Force the control lines
    ** low and the chip selects high.
    */
    *QPAR = 0;
    *QPDR &= (cs_mask | ~cntl_mask);
    *QDDR |= (cs_mask | cntl_mask);    
}

static void
bailout(void)
{
    nv_value	nv;

    PET_WATCHDOG();

    fputs("Cleaning up, please wait ... ", stderr);
    fflush(stderr);
    
    closelog();
    iop_clear_all();
    
    nv.l = 1;
    nv_insert("power-cycle", &nv, NV_INT, 1);

    fputs("done\n", stderr);
    DelayMilliSecs(500L);
    
    /*
    ** The infinite loop prevents gcc from thinking the function
    ** does return even though it was declared "noreturn".
    */
    while(1)
	Reset();
}


long nr_spurious_ints;

static void
spur_int_handler(void)
{
    nr_spurious_ints++;
}


/*
 * Try to HOME the piston "N" times.  If the operation fails, manually set
 * the piston position to "x" centimeters.
 */
void
try_home(int N, double x)
{
    while(motor_home(900L) < 0 && N-- > 0)
	log_event("motor HOME process interrupted.  %s\n",
		  (N == 0) ? "Skipping" : "Trying again");
	
    if(N <= 0)
    {
	log_event("Setting current motor position to %.2f cm\n", x);
	motor_set_position(cm_to_counts(x));
    }

}

INITFUNC(init_safemode)
{
    InitTT8(NO_WATCHDOG, TT8_TPU);
    InitCF8(CF8StdCS, CF8StdAddr);
    if(errno != 0)
	printf("\nWARNING: InitCF8 failed, error %d\n", errno);
    if(errno == -1 || errno == PiDosNoHardware)
	exit(1);

    /*
    ** Initialize non-volatile RAM and the I/O ports
    */
    init_nvram();
    init_ioports();
    
}

int
main(void)
{
    time_t		secs;
    struct tm		*now;
    long		mission_length;
    int			initial_comm;
    nv_value		nv[2];
    static ExcCFrame	eframe;
    
    /* Catch spurious interrupts */
    InstallHandler(spur_int_handler, Spurious_Interrupt, &eframe);
    SimSetFSys(16000000L);

    mlf2_qspi_init(); 

    printf("\n\nMLF2 Safe-mode Program (%s, %s)\n", __DATE__, __TIME__);
    printf("Revision: %s\n\n", REVISION);

    /*
    ** Initialize the clock
    */
    secs = RtcToCtm();
    now = localtime(&secs);
    SetTimeTM(now, NULL);

    appendlog("safelog.txt");
    init_sleep_hooks();
    
    PET_WATCHDOG();

    log_event("Software revision: %s\n", REVISION);
    log_event("Library revision: %s\n", MLF2LIB_REVISION);

    /*
    ** Use GPS to set system clock.
    */
    if(gps_init())
    {
	gps_set_clock();
	gps_shutdown();
    }
    
    /* 
    ** Open mission control file, load and display the parameters.
    */
    if((mission_length = mission_read("safemode.xml")) <= 0)
	mission_length = 86400;
	
    log_event("Safe-mode duration %ld seconds\n", mission_length);    

    /*
    ** Read "extra" parameters.
    */
    params_read("metadata.xml");

    fputs("Entering SAFE MODE in 30 seconds, <CR><CR> to abort\n",
	  stdout);
    if(isleep(30L) != 0)
	bailout();

    initial_comm = 0;
    
    if(NV_ISSET("piston", &nv[0]))
    {
	log_event("using saved piston position, %ld counts\n", nv[0].l);
	motor_set_position(nv[0].l);
	nv_delete("piston");
	nv_write();

	/*
	 * If the saved piston position is present that means the float was commanded
	 * to enter safe mode and thus is still on the surface so we will perform an
	 * initial COMM mode.
	 */
	initial_comm = 1;
    }
    else
	try_home(5, (double)15.);

    PET_WATCHDOG();
    
    if(cpu_clock > 16 || cpu_clock <= 1)
	cpu_clock = 16;
    log_event("Setting CPU clock to %ld mhz\n", cpu_clock);
    SimSetFSys(cpu_clock*1000000L);
    SerSetBaud(9600L, 0L);

    /*
    ** Set the power-cycle flag to 0 which will force the float into RECOVERY
    ** mode if it is reset.
    */
    log_event("Clearing power-cyle flag\n");
    nv[0].l = 0L;
    nv_insert("power-cycle", &nv[0], NV_INT, 0);
    nv_write();

    newpr_init();

    if(initial_comm)
    {
	log_event("Calling home before starting safe-mode\n");
	if(fileexists(MESSAGE_FILE))
	    unlink(MESSAGE_FILE);
	mq_add("Safe Mode");
	comm_mode("status", 0);
    }

    mlf2_main_loop(MODE_START, mission_length);
    
    bailout();
    
    return 0;
}
