/*
** $Id: comm.h,v 30ef1374b225 2007/04/15 03:14:25 mikek $
*/
#ifndef _COMM_H_
#define _COMM_H_

#ifndef WATCHDOG_TIMEOUT
#define WATCHDOG_TIMEOUT	900L
#endif

#ifndef __GNUC__
#define __attribute__(x)
#endif

#define TELEM_LIST	"tlist.txt"

#define do_abort	abort_mission

void comm_mode(char *type, int gps_on);
void comm_wait(int gps_on);
void comm_recovery_mode(void) __attribute__ ((noreturn));

#endif
